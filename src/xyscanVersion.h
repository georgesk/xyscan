//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2022 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: June 15, 2023
//-----------------------------------------------------------------------------
#ifndef xyscanVersion_h
#define xyscanVersion_h
#define VERSION "4.6.6"
#define VERSION_NUMBER 4.66
#endif

