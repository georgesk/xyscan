//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2020 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: September 6, 2020
//-----------------------------------------------------------------------------
#include <QtGui>
#include <QApplication>
#include <iostream>
#include <QString>
#include <QFileInfo>
#include "xyscanWindow.h"
#include "xyscanVersion.h"

using namespace std;

//#define TEST_FRENCH_VERSION 1
//#define TEST_GERMAN_VERSION 1

int main(int argc, char *argv[])
{
    //
    //  Init application
    //
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(xyscan);
    app.setApplicationVersion(VERSION);
    app.setApplicationName("xyscan");

    //
    //  Internationalization
    //  Load translator unless we are using English.
    //  If no translator exist for the language of the host
    //  system we fall back to English.
    //
#ifdef TEST_FRENCH_VERSION
    QLocale::setDefault(QLocale(QLocale::French, QLocale::France));
#elif TEST_GERMAN_VERSION
    QLocale::setDefault(QLocale(QLocale::German, QLocale::Germany));
#endif
    QLocale locale;

    QTranslator qtTranslator, qtBaseTranslator, xyscanTranslator;

    if (locale.language() != QLocale::English) {
        if (qtTranslator.load(locale, "qt", "_", ":/translations")) app.installTranslator(&qtTranslator);
        if (qtBaseTranslator.load(locale, "qtbase", "_", ":/translations")) app.installTranslator(&qtBaseTranslator);
        if (xyscanTranslator.load(locale, "xyscan", "_", ":/translations")) app.installTranslator(&xyscanTranslator);
    }

    //
    //  Create and launch xyscan
    //
    xyscanWindow win;
    app.installEventFilter(&win);
    win.show();
    return app.exec();
}


