//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2022 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//
//  Author: Thomas S. Ullrich
//  Last update: Jan 10, 2022
//-----------------------------------------------------------------------------
#ifndef xyscanDataTable_h
#define xyscanDataTable_h
#include <QTableWidget>
#include <QTextDocument>
#include <vector>

class QImage;
class QFile;
class QShortcut;

using namespace std;

class xyscanDataTable : public QTableWidget
{
    Q_OBJECT

public:
    xyscanDataTable(QWidget *);
    
    void addPoint(double, double, vector<pair<double, double>>, vector<pair<double, double>>, unsigned int);

    bool dataSaved() const;
    void setDataSaved(bool);
    
    QString userComment() const;
    void setUserComment(const QString&);

    void addColumnsForXError();
    void addColumnsForYError();
    void resetColumns();
    void resetAll();

    void writePrinterDocument(QTextDocument&, const QString&, const QImage* = nullptr);
    void writeRootMacro(QFile&, const QString&, bool, bool, double mc[4]);
    void writeTextFile(QFile&, const QString&);
    void writeCsvFile(QFile&, const QString&);

    double value(int, int) const;
    double x(int) const;
    double y(int) const;
    double dx_lower(int, int) const;
    double dx_upper(int, int) const;
    double dy_lower(int, int) const;
    double dy_upper(int, int) const;

public slots:
    void deleteLast();
    void deleteAll();
    
private:
    void toggleTableFormat();
    void toggleTableAlignment();

private:
    QString mUserComment;
    int mXErrorScanCount;
    int mYErrorScanCount;
    bool mDataSaved;

    QShortcut *mTableFormattingShortcut;
    QShortcut *mTableAlignmentShortcut;
};

inline bool xyscanDataTable::dataSaved() const {return mDataSaved;}
inline void xyscanDataTable::setDataSaved(bool val) {mDataSaved = val;}
inline QString xyscanDataTable::userComment() const {return mUserComment;}
inline void xyscanDataTable::setUserComment(const QString& str) {mUserComment = str;}
inline double xyscanDataTable::x(int i) const {return value(i, 0);}
inline double xyscanDataTable::y(int i) const {return value(i, 1);}

#endif
