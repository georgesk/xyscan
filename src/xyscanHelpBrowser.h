//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2015 Thomas S. Ullrich 
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: July 30, 2014
//-----------------------------------------------------------------------------
#ifndef xyscanHelpBrowser_h
#define xyscanHelpBrowser_h
 
#include <QWidget>
#include <QMap>

class QPushButton;
class QTextBrowser;
class QTreeWidget;
class QTreeWidgetItem;

class xyscanHelpBrowser :  public QWidget
{
    Q_OBJECT
    
public:
    xyscanHelpBrowser(const QString &, const QString &);
 
private slots:
    void updateCaption();
    void itemSelected(QTreeWidgetItem* item);
    void goHome();
    void goForward();
    void goBackward();
    
private:
    void createIndex(const QString &);
    void showPage(const QString &);
    void sync();

private:
    QTextBrowser *mTextBrowser;
    QTreeWidget  *mTreeWidget;
    QPushButton  *mHomeButton;
    QPushButton  *mBackButton;
    QPushButton  *mForwardButton;
    QPushButton  *mCloseButton;
    QMap<QString, QTreeWidgetItem*> mMap;
};
#endif


