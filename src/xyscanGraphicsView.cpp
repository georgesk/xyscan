//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2015 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//
//  Author: Thomas S. Ullrich
//  Last update: April 20, 2015
//-----------------------------------------------------------------------------

#include <QUrl>
#include <QMimeData>
#include "xyscanGraphicsView.h"
#include <iostream>
using namespace std;

xyscanGraphicsView::xyscanGraphicsView (QWidget* parent) : QGraphicsView(parent)
{
    setAcceptDrops(true);
}

void xyscanGraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void xyscanGraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void xyscanGraphicsView::dropEvent(QDropEvent *event)
{
    for (const QUrl &url : event->mimeData()->urls()) {
        const QString &fileName = url.toLocalFile();
        emit fileDropped(fileName);
    }
    event->acceptProposedAction();
}
