//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2024 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: Feb 16, 2024
//-----------------------------------------------------------------------------
#include "xyscanAbout.h"
#include "xyscanVersion.h"
#include <iostream>
#include <QtGui>
#include <QFrame>
#include <QPushButton>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScreen>
#include <QGuiApplication>

using namespace std; 

xyscanAbout::xyscanAbout(QWidget* parent)
    : QDialog(parent),
      mTitlePixmap(":/images/xyscanSplash.png"),
      mVersionText(QString(tr("Release ")) + QString(VERSION) + QString(" (") + QString(__DATE__) + QString(")"))
{
    setModal(true);

    //
    //  Get screen
    //
    QScreen *screen = QGuiApplication::primaryScreen();

    //
    //  Change icons for high-resolution displays
    //
    if (screen->devicePixelRatio() > 1) {
        mTitlePixmap.load(":/images/xyscanSplash@2x.png");
        mTitlePixmap.setDevicePixelRatio(2.0);
    }
    
    //
    //  Merge version/release string into pixmap
    //
    QPainter p;
    int leftTextEdge = 341;
    p.begin( &mTitlePixmap );
    int strWidth = p.fontMetrics().horizontalAdvance( mVersionText );
    int strHeight = p.fontMetrics().height();
    p.setPen(Qt::gray);
    p.drawText(QRect(leftTextEdge, 98, strWidth, strHeight), 0, mVersionText );

    //
    //  Merge copyright string into pixmap
    //
    QFont copyrightFont("Arial", 16);
    QString mCopyrightText("Copyright 2002-2024\nThomas S. Ullrich");
    strWidth = p.fontMetrics().horizontalAdvance( mCopyrightText );
    strHeight = p.fontMetrics().height();
    p.setPen(Qt::white);
    p.setFont(copyrightFont);
    p.drawText(QRect(leftTextEdge, 152, strWidth, 4*strHeight), 0, mCopyrightText );
    p.end(); 
    
    //
    //  First define the pixmap label
    //
    int w = mTitlePixmap.width()/screen->devicePixelRatio();
    int h = mTitlePixmap.height()/screen->devicePixelRatio();
    
    mTitlePixmapLabel = new QLabel(this);

    mTitlePixmapLabel->setGeometry(QRect( 0, 0, w, h));
    mTitlePixmapLabel->setFixedSize(QSize(w, h));
    mTitlePixmapLabel->setLineWidth(0);
	mTitlePixmapLabel->setPixmap(mTitlePixmap);

    mTitlePixmapLabel->setScaledContents(false);
    mTitlePixmapLabel->setAlignment(Qt::AlignLeft|Qt::AlignTop);
    mTitlePixmapLabel->setVisible(true);

    //
    //  Now the license label
    //
    mTitleLicenseLabel = new QLabel(this);
    mTitleLicenseLabel->setGeometry(QRect( 0, 0, w, h));
    mTitleLicenseLabel->setFixedSize(QSize(w, h));
    mTitleLicenseLabel->setLineWidth(0);
    mTitleLicenseLabel->setScaledContents(false);
    mTitleLicenseLabel->setAlignment(Qt::AlignLeft|Qt::AlignTop);
    mTitleLicenseLabel->setIndent(10); // for text
    mTitleLicenseLabel->setText(tr(
                                  "Copyright (C) 2002-2024 Thomas S. Ullrich\n"
                                  "\n"
                                  "This program is free software; you can redistribute it and/or\n"
                                  "modify it under the terms of the GNU General Public License as\n"
                                  "published by the Free Software Foundation; either version 3 of\n"
                                  "the License, or any later version.\n"
                                  "\n"
                                  "This program is distributed  in the hope that it will be useful,\n"
                                  "but without any warranty; without even the implied warranty of\n"
                                  "merchantability or fitness for a particular purpose. See the GNU\n"
                                  "General Public License for more details.\n"
                                  "\n"
                                  "You should have received a copy of the GNU General Public\n"
                                  "License along with this program. If not, see <http://www.gnu.org/licenses/>."
                                  ));
    mTitleLicenseLabel->setVisible(false);

    //
    //  Now the line (just looks nicer with it)
    //
    mLine = new QFrame(this);
    mLine->setGeometry(QRect(0, h, w, 5));
    mLine->setFrameShape(QFrame::HLine);
    mLine->setFrameShadow(QFrame::Sunken);

    //
    //  The buttons
    //
    mButtonOk = new QPushButton(this);
    mButtonOk->setAutoDefault(true);
    mButtonOk->setDefault(true);
    mButtonOk->setText( tr("&Ok" ));
    mButtonLicense = new QPushButton(this);
    mButtonLicense->setText(tr("&License"));

    //
    //  Then the dialog itself
    //
    setWindowTitle(tr( "About xyscan"));
    setSizeGripEnabled(false);
    setFixedWidth(w);

    //
    //  Layout the buttons
    //
    QSpacerItem* spacer = new QSpacerItem (300,20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    mButtonLayout = new QHBoxLayout;
    mButtonLayout->addWidget(mButtonLicense);
    mButtonLayout->addItem(spacer);
    mButtonLayout->addWidget(mButtonOk);
    mButtonLayout->setContentsMargins(6,4,6,4);
    //
    //  Overall widget layout
    //
	mLayout = new QVBoxLayout(this);
	mLayout->setSpacing(0);
    mLayout->setContentsMargins(0,0,0,0);
    mLayout->addWidget(mTitlePixmapLabel);
    mLayout->addWidget(mLine);
    mLayout->addLayout(mButtonLayout);

    setFixedSize(mLayout->minimumSize());
   
    //
    //  Connect slots
    //
    connect( mButtonOk, SIGNAL(clicked()), this, SLOT(accept()));
    connect( mButtonLicense, SIGNAL(clicked()), this, SLOT(toggleTitle()));
    
    //
    //  We start with the pixmap title
    //
    mTitlePixmapShown = true;

}

void xyscanAbout::accept()  // Ok button pressed
{
    //
    // Hide it and make sure the next time it pops
    // up the title page is shown.
    //
    hide();
    if (!mTitlePixmapShown) toggleTitle();
}

void xyscanAbout::toggleTitle()  // License button pressed
{
    if (mTitlePixmapShown) {
        mButtonLicense->setText(tr("Tit&le && Version"));
        qApp->processEvents();  // needed since Qt5.11
        mLayout->replaceWidget(mTitlePixmapLabel, mTitleLicenseLabel);
    }
    else {
        mButtonLicense->setText(tr("&License"));
        qApp->processEvents();   // needed since Qt5.11
        mLayout->replaceWidget(mTitleLicenseLabel, mTitlePixmapLabel);
    }
    mTitlePixmapLabel->setVisible(!mTitlePixmapShown);
    mTitleLicenseLabel->setVisible(mTitlePixmapShown);
    mTitlePixmapShown = !mTitlePixmapShown;
    update();
}


