//-----------------------------------------------------------------------------
//  Copyright (C) 2020 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//
//  Author: Thomas S. Ullrich
//  Last update: March 4, 2020
//-----------------------------------------------------------------------------
#ifndef xyscanErrorBarScanModeToolBox_h
#define xyscanErrorBarScanModeToolBox_h

#include <QObject>
#include <QVector>

class QWidget;
class QPushButton;
class QGridLayout;
class QComboBox;

enum class xyscanErrorScanMode {mNone=0, mAsymmetric, mAverage, mMax};

class xyscanErrorBarScanModeToolBox : public QObject {

    Q_OBJECT

public:
    xyscanErrorBarScanModeToolBox(QWidget*);
    
    void reset();
    void disable();
    void enable();

    void addXErrorComboBox();
    void addYErrorComboBox();

    void removeLastXErrorComboBox();
    void removeLastYErrorComboBox();

    int numberOfXErrorScans() const;
    int numberOfYErrorScans() const;

    xyscanErrorScanMode scanModeX(int) const;
    xyscanErrorScanMode scanModeY(int) const;

public slots:
    void update();

private:
    xyscanErrorScanMode modeFromIndex(int, int) const;

private:
    QWidget *mForm;
    
    QVector<QComboBox*> mErrorXModeComboBoxVector;
    QVector<QComboBox*> mErrorYModeComboBoxVector;

    QPushButton* mErrorXModePlusPushButton;
    QPushButton* mErrorYModePlusPushButton;
    QPushButton* mErrorXModeMinusPushButton;
    QPushButton* mErrorYModeMinusPushButton;

    QGridLayout *mErrorXModeLayout;
    QGridLayout *mErrorYModeLayout;
};
#endif

