//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2017 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: October 12, 2017
//-----------------------------------------------------------------------------
#ifndef xyscanUpdater_h
#define xyscanUpdater_h

#include <QNetworkAccessManager>
#include <QUrl>

class QNetworkReply;

class xyscanUpdater : public QObject
{   
    Q_OBJECT

public:
    xyscanUpdater();
    
    void checkForNewVersion(const QUrl&);
    void setNextCheckIsSilent(bool);

signals:
    void userReminded();
    
public slots:
    void downloadFinished(QNetworkReply *reply);    
    
private:
    QNetworkAccessManager  mManager;
    QNetworkReply         *mCurrentDownload;
    
    bool mNextCheckIsSilent;
};

inline void xyscanUpdater::setNextCheckIsSilent(bool val) {mNextCheckIsSilent = val;}

#endif
