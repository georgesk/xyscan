xyscan (4.66-1) unstable; urgency=medium

  * New upstream version 4.66

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 06 Mar 2024 15:31:10 +0100

xyscan (4.64-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Use secure URI in Homepage field.
  * Change priority extra to priority optional.
  * Use canonical URL in Vcs-Browser.
  * Remove deprecated Encoding key from desktop file debian/xyscan.desktop.
  * Update standards version to 4.6.0, no changes needed.

  [Georges Khaznadar]
  * bumped Standards-Version: 4.6.2

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 21 Feb 2023 19:51:14 +0100

xyscan (4.64-1) unstable; urgency=medium

  * updated watch file
  * New upstream version 4.64

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 25 Apr 2022 18:56:45 +0200

xyscan (4.61-1) unstable; urgency=medium

  * New upstream version 4.61
  * upgraded debhelper-compat (=13), Standards-Version: 4.5.1
  * removed debian patches, they became useless.

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 22 Jan 2022 18:40:47 +0100

xyscan (4.50-1) unstable; urgency=medium

  * New upstream version 4.50
  * upgraded to debhelper-compat = 12, Standards-Version: 4.5.0
  * updated debian patches
  * added a build-depedency on libqt5charts5-dev
  * modified the installation and cleanup of .qm translation files

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 24 Aug 2020 14:44:53 +0200

xyscan (4.30-2) unstable; urgency=medium

  * applied  Helmut Grohne's patch. Closes: #902184
    + Let dh_auto_configure run cross qmake.
    + Missing Build-Depends: qt5-qmake:native for lrelease

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 23 Jul 2018 16:39:07 +0200

xyscan (4.30-1) unstable; urgency=medium

  * updated the file debian/watch to reflect upstream's location change
  * upgraded to the newest upstream source
  * upgraded Standards-Version to 4.1.3, dh to 10
  * added Vcs fields for d/control

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 24 Oct 2017 17:11:51 +0200

xyscan (4.21-1) unstable; urgency=medium

  * New upstream release
  * updated Standards-Version
  * removed the dependency on qt-default, replaced it by
    export QT_SELECT=qt5 (in d/rules), added other dependencies
    on qt5 dev stuff.
  * added the build dependency libpoppler-qt5-dev
  * changed /usr/local paths to /usr in xyxscan.pro

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 24 Oct 2017 11:21:05 +0200

xyscan (3.33-1) unstable; urgency=medium

  * upgraded to the newest upstream version
  * updated Standards-Version to 3.9.5 dependency on debhelper >= 9
    and debian/compat to 9
  * changed to build-depependencies on qtbase5-dev, qt5-qmake and
    added a switch -qt=5 to qmake call, and modified the install path
    of translations accordingly.
  * added buid dependencies on qtmultimedia5-dev, qttools5-dev-tools
  * changed my DEBEMAIL to georgesk@debian.org
  * added hardening support
  * added a dependency on qttranslations5-l10n

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 26 Aug 2014 14:03:59 +0200

xyscan (3.31-3) unstable; urgency=low

  * modified the short description. Closes: #634332

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 18 Jul 2011 23:20:49 +0200

xyscan (3.31-2) unstable; urgency=low

  * fixed debian/control. Closes: #629376

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 06 Jun 2011 08:47:17 +0200

xyscan (3.31-1) unstable; urgency=low

  * upgraded to the newest upstream version
  * updated a few French strings
  * modified the installation path for xyscan_fr.qm

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 02 Jun 2011 20:36:43 +0200

xyscan (3.22-2) unstable; urgency=low

  * added support for i18n and a French translation

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 29 May 2011 21:59:43 +0200

xyscan (3.22-1) unstable; urgency=low

  * Initial release (Closes: #628421)
  * added a manpage and desktop file
  * modifed the method to access the docs directory

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 28 May 2011 20:16:50 +0200
