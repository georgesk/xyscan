=======================================================================
xyscan 4
=======================================================================
A data thief for scientist.

Copyright 2002-2023 Thomas S. Ullrich (thomas.ullrich@bnl.gov)
This version: 4.6.6

xyscan is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; See license.txt and gpl.txt.

This directory contains all sources and files needed to built
xyscan. Supported platforms are LINUX, Windows, and Mac OS X.


Obtaining xyscan
================
Binary distributions are available from the xyscan web site:
http://rhig.physics.yale.edu/~ullrich/software/xyscan/index.html
If not available for your system you need to build it from 
the source distribution available on the same side.
For Windows or Mac I strongly recommend to grab the installer
and use the binary distribution. Building an app on either platform
is not for the faint hearted. Note that I have not updated the
Window version since a long time. On Linux one has to build
xyscan from the source.

Building xyscan on Unix/Linux
=============================
This is straightforward. All what is needed is a C++ compiler, Qt6, and
the poppler library used for handling PDFs (poppler-qt5 is needed).

You can either use the command line tool qmake or the Qt Creator.
Both are shipped with Qt.


1. Download the tar ball of the latest source
   (here xyscan-4.66-src.tgz)

2. Unpack
   tar -xzvf xyscan-4.66-src.tgz

   This will create a directory xyscan with all the needed files

3. cd xyscan

4. Adjusting the xyscan.pro file to your installation.

   Edit xyscan.pro. There are very few places where you might
   need to adjust things:

   a) Check where your “poppler” library and include files are installed.
      If they are not in /usr/local or /opt/local or /usr you will need 
      to change the referring lines.
   b) By default xyscan will be installed in /usr/local/bin
      and the needed resource files (documentation) in 
      /usr/local/share/xyscan. If you want it in a different place
      use qmake PREFIX=<your directory> 
      
      Note that you might have to edit xyscanWindow.cpp and modify 
      createHelpBrowser() to accommodate any unusual location(s).

5. qmake [PREFIX=<install-dir>] xyscan.pro

6. make

7. make install
   or 
   sudo make install
   depending on your privileges 

8. The handling of application icons varies from distribution
   to distribution. In the icons/ directory you will find a
   number of icons in different sizes that you can use.

