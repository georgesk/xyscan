<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>xyscanAbout</name>
    <message>
        <source>Copyright (C) 2002-2020 Thomas S. Ullrich

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of
the License, or any later version.

This program is distributed  in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</source>
        <translation type="vanished">Copyright (C) 2002-2020 Thomas S. Ullrich

Ce programme est un logiciel libre ; vous pouvez le redistribuer
ou le modifier suivant les termes de la GNU General Public License
telle que publiée par la Free Software Foundation , soit la version 3
de la Licence, soit (à votre gré) toute version ultérieure.

Ce programme est distribué dans l’espoir qu’il sera utile,
mais SANS AUCUNE GARANTIE: sans même la garantie implicite
de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF
PARTICULIER. Consultez la GNU General Public License pour 
plus de détails.

Vous devriez avoir reçu une copie de la GNU General Public 
License avec ce programme ; si ce n’est pas le cas, consultez:
&lt;http://www.gnu.org/licenses/&gt;.</translation>
    </message>
    <message>
        <location filename="../src/xyscanAbout.cpp" line="+86"/>
        <source>Copyright (C) 2002-2021 Thomas S. Ullrich

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of
the License, or any later version.

This program is distributed  in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</source>
        <translation>Copyright (C) 2002-2021 Thomas S. Ullrich

Ce programme est un logiciel libre ; vous pouvez le redistribuer
ou le modifier suivant les termes de la GNU General Public License
telle que publiée par la Free Software Foundation , soit la version 3
de la Licence, soit (à votre gré) toute version ultérieure.

Ce programme est distribué dans l’espoir qu’il sera utile,
mais SANS AUCUNE GARANTIE: sans même la garantie implicite
de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF
PARTICULIER. Consultez la GNU General Public License pour 
plus de détails.

Vous devriez avoir reçu une copie de la GNU General Public 
License avec ce programme ; si ce n’est pas le cas, consultez:
&lt;http://www.gnu.org/licenses/&gt;.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+62"/>
        <source>&amp;License</source>
        <translation>&amp;License</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>About xyscan</source>
        <translation>A propos d&apos;xyscan</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Tit&amp;le &amp;&amp; Version</source>
        <translation>Tit&amp;re &amp;&amp; Version</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Release </source>
        <translation>Version du logiciel </translation>
    </message>
</context>
<context>
    <name>xyscanBaseWindow</name>
    <message>
        <location filename="../src/xyscanBaseWindow.cpp" line="+151"/>
        <source>No image loaded</source>
        <translation>Aucune figure chargée</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Rotate:</source>
        <translation>Rotation:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Scale:</source>
        <translation>Echelle:</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Lower x:</source>
        <translation>x inférieur:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+3"/>
        <location line="+3"/>
        <location line="+3"/>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Upper x:</source>
        <translation>x supérieur:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lower y:</source>
        <translation>y inférieur:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Upper y:</source>
        <translation>y supérieur:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Log x</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Log y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Lin/Log Scale:</source>
        <translation>Échelle Lin/Log:</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+4"/>
        <location line="+23"/>
        <location line="+4"/>
        <location line="+295"/>
        <location line="+18"/>
        <location line="+18"/>
        <location line="+22"/>
        <location line="+27"/>
        <source>N/A</source>
        <translation>-</translation>
    </message>
    <message>
        <location line="-354"/>
        <source>Pixel:</source>
        <translation>En pixel:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+3"/>
        <source>x:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+3"/>
        <source>y:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Plot Coordinates:</source>
        <translation>A l&apos;échelle du graphe:</translation>
    </message>
    <message>
        <source>Cannot find the directory holding the help files (docs). No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="obsolete">Le répertoire contenant la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <source>Cannot find the index descriptor to setup the help system. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="obsolete">Le fichier d&apos;index de la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Cannot find the directory holding the help files. No help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Le répertoire contenant la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot find the index descriptor to setup the help system. No help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Le fichier d&apos;index de la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Density Histogram</source>
        <translation>Histogramme de densité</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Grayscale Densities Around Cursor</source>
        <translation>Densité de niveaux de gris autour du curseur</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Horizontal and Vertical </source>
        <translation>Horizontal et vertical </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Horizontal Only </source>
        <translation>Horizontal seulement </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Vertical Only </source>
        <translation>Vertical seulement </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Density Histogram</source>
        <translation>Histogramme de &amp;densité</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Start Measuring</source>
        <translation>Commencer à mesurer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>dx:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>dy:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>r:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Radians</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>L:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Units:</source>
        <translation>Unité:</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scale:</source>
        <comment>short</comment>
        <translation>Echelle:</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pixel</source>
        <translation>Pixel</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plot Units</source>
        <translation>Graphe</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Scale</source>
        <translatorcomment>Échelle utilisateur</translatorcomment>
        <translation>Personnalisée</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Clear History</source>
        <translation>Effacer l&apos;&amp;historique</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Save...</source>
        <translation>&amp;Enregistrer...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Print...</source>
        <translation>&amp;Imprimer...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Quit xyscan</source>
        <translation>&amp;Quitter xyscan</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Paste Image</source>
        <translation>&amp;Coller une image</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Crosshair Color...</source>
        <translation>Couleur de la croix...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Marker Color...</source>
        <translation>Couleur du marqueur...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Path Color...</source>
        <translation>Couleur du chemin...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Delete &amp;Last Point</source>
        <translation>Effacer &amp;le dernier point</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Table</source>
        <translation>Clair tableau de valeurs</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Comment...</source>
        <translation>&amp;Commentaires...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Current Precision</source>
        <translation>Pré&amp;cision</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scan Screen</source>
        <translation>Écran du scan</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About xyscan</source>
        <translation>&amp;A propos d&apos;xyscan</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>xyscan &amp;Help</source>
        <translation>&amp;Aide xyscan (en anglais)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Tool Tips</source>
        <translation>&amp;Bulles d&apos;aide</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Check For Updates ...</source>
        <translation>&amp;Vérifier les mises à jour...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;Recent</source>
        <translation>Ouvrir &amp;Récents</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Significant Digits</source>
        <translation>Chiffres significatifs</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;View</source>
        <translation>A&amp;ffichage</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Data Table</source>
        <translation>Tableau de Valeurs</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Data Table</source>
        <translation>&amp;Tableau de Valeurs</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Image Info and Adjustment</source>
        <translation>Info et ajustement de l&apos;image</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Axis Settings</source>
        <translation>Réglage des axes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error Bar Scan Mode</source>
        <translation>Mode de scan de barres d&apos;erreur</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Coordinate Display</source>
        <translation>Affichage des coordonnées</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Measure Tool</source>
        <translation>Outil de mesure</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Ctrl</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Scan area. The loaded graphics
is displayed here.</source>
        <translation>Aire du scan. La figure que vous avez
chargée est visible ici.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Help and Documentation for xyscan.</source>
        <translation>Aide et documentation pour xyscan.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Status bar. Displays instruction during scanning.</source>
        <translation>Barre de statut. Affiche des instructions pendant le scan.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Horizontal bar of crosshairs cursor.</source>
        <translation>Axe horizontal du curseur.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Vertical bar of crosshairs cursor.</source>
        <translation>Axe vertical du curseur.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for lower x-axis position.</source>
        <translation>Marqueur pour la position inférieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for upper x-axis position.</source>
        <translation>Marqueur pour la position supérieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for lower y-axis position.</source>
        <translation>Marqueur pour la position inférieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for upper y-axis position.</source>
        <translation>Marqueur pour la position supérieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Measure Tool Window:
Allows to measure distances and paths in plots and maps.</source>
        <translation>Outil de mesure:
Permet de mesurer les distances et les chemins dans les graphes et cartes.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Starts and stops measuring.</source>
        <translation>Commence et arrȇte de mesurer.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>If checked, will measure along a path (polyline)
instead measuring a point-to-point distance.
The points on the path are set by double-clicking
the mouse.</source>
        <translation>Si cette case est cochée, la mesure se fera le long 
d&apos;un chemin (polyligne) au lieu de mesurer une 
distance point à point. Les points de la trajectoire 
sont définis par un double clic sur la souris.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>While measuring, displays the horizontal distance (dx).</source>
        <translation>Pendant la mesure, affiche la distance horizontale (dx).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the vertical distance (dy).</source>
        <translation>Pendant la mesure, affiche la distance verticale (dy).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the radial distance (dr).</source>
        <translation>Pendant la mesure, affiche la distance absolue (dr).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the angle w.r.t. the horizontal axis.</source>
        <translation>Pendant la mesure, affiche l&apos;angle par rapport à l&apos;axe horizontal.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select if angles should be displayed in radians
instead of degrees.</source>
        <translation>Sélectionnez si les angles doivent être affichés en radians
au lieu de degrés.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit Selector:
Select the units in which the distance is displayed.</source>
        <translation>Choix de l&apos;unité:
Sélectionnez l&apos;unité dans laquelle la distance est affichée.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User Scale:
Allows to enter a scale in which the measured
distances will be expressed. The shown distances
are calculated by multiplying this scale by the
measured length in pixel. If you have an object in
the plot of which you know the length in your desired
units (e.g., cm, miles) you can measure it using the
measure tool (let it sit at the right distance) and
then enter the length of that object in its own units
and press Shift+Enter. This will then automatically
calculate, set, and display the correct scale.</source>
        <translation>Échelle de l&apos;utilisateur :
Permet de saisir une échelle dans laquelle les distances mesurées
distances mesurées seront exprimées. Les distances affichées
sont calculées en multipliant cette échelle par la
longueur mesurée en pixel. Si vous avez un objet dans
objet dont vous connaissez la longueur dans l&apos;unité de votre choix (par ex. cm, miles), vous pouvez la mesurer à l&apos;aide de l&apos;échelle de mesure.
unités souhaitées (par exemple, cm, miles), vous pouvez le mesurer à l&apos;aide de l&apos;outil de mesure (laissez-le à la bonne distance).
outil de mesure (laissez-le à la bonne distance) et
puis saisir la longueur de cet objet dans ses propres unités.
et appuyez sur Shift+Enter. Cela permettra alors de
calculera, définira et affichera automatiquement l&apos;échelle correcte.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Shows the measured path length.</source>
        <translation>Indique la longueur du chemin mesurée.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resets the path length and deletes the path
superimposed on the image.</source>
        <translation>Réinitialise la longueur de la trajectoire et supprime la trajectoire
superposé à l&apos;image.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cursor Zoom Window:
Displays a zoomed view of the area
surrounding the cross-hair allowing
for higher scan precision.</source>
        <translation>Curseur de la fenêtre d&apos;agrandissement:
Affiche une vue agrandie de la région autour
du curseur pour une plus grande précision
du scan.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set the zoom scale.</source>
        <translation>Réglage de l&apos;échelle du zoom.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Histogram Window:
Displays a histogram that indicates the
grayscale densities around the cursor
position.</source>
        <translation>Fenêtre d&apos;histogramme:
Affiche l&apos;histogramme qui indique la
densités de niveaux de gris autour de
la position du curseur.</translation>
    </message>
    <message>
        <source>Histogram Window:
Displays histogram that indicate the
grayscale densities around the cursor
position.</source>
        <translation type="obsolete">Fenêtre d&apos;histogramme:
Affiche l&apos;histogramme qui indique la
densités de niveaux de gris autour de
la position du curseur.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Selects which data to show in the histogram.</source>
        <translation>Sélectionne les données à afficher dans l&apos;histogramme.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Coordinates Window:
Displays cursor position in local
(pixel) and plot coordinates.
Shortcut is %1+C.</source>
        <translation>Fenêtre d&apos;affichage des coordonnées:
Coordonnées locales (en pixel)
et à l&apos;échelle du graphe.
Le raccourci est %1+C.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Displays the x coordinate of the cursor in pixel (screen) units.</source>
        <translation>Affiche l&apos;abscisse x du curseur en pixel.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Displays the y coordinate of the cursor in pixel (screen) units.</source>
        <translation>Affiche l&apos;ordonnée y du curseur en pixel.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Displays the x coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation>Affiche l&apos;abscisse x du curseur avec l&apos;échelle définie.
Cette valeur est enregistrée dans le tableau de valeurs
en appuyant sur la touche espace.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Displays the y coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation>Affiche l&apos;ordonnée y du curseur avec l&apos;échelle définie.
Cette valeur est enregistrée dans le tableau de valeurs
en appuyant sur la touche espace.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Axis Settings Window:
Use to set axes marker
and set log/lin scales.
Shortcut is %1+A.</source>
        <translation>Fenêtre de réglage des axes:
À utiliser pour régler le maquage des axes
et choisir les échelles log/lin.
Le raccourci est %1+A.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set value of marker for the lower x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+1.</source>
        <translation>Entrer la valeur du marqueur pour la position
inférieure de l&apos;axe des abscisses x. Une boite de
dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du 
graphique. Le raccourci est %1+1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the upper x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+2.</source>
        <translation>Entrer la valeur du marqueur pour la position
supérieure de l&apos;axe des abscisses x. Une boite
de dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du
graphique. Le raccourci est %1+2.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the lower y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+3.</source>
        <translation>Entrer la valeur du marqueur pour la position inférieure
de l&apos;axe des ordonnées y. Une boite de dialogue apparait
pour la saisie de la valeur correspondante dans les
coordonnées du graphique. Le raccourci est %1+3.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the upper y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+4.</source>
        <translation>Entrer la valeur du marqueur pour la position
supérieure de l&apos;axe des ordonnées y. Une boite
de dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du
graphique. Le raccourci est %1+4.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>x-axis value in plot coordinates assigned to the low-x marker (read only).</source>
        <translation>Valeur de l&apos;axe-x dans le graphique affectée au marqueur inférieur pour x (lecture seule).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>x-axis value in plot coordinates assigned to the upper-x marker (read only).</source>
        <translation>Valeur de l&apos;axe-x dans le graphique affectée au marqueur supérieur pour x (lecture seule).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>y-axis value in plot coordinates assigned to the low-y marker (read only).</source>
        <translation>Valeur de l&apos;axe-y dans le graphique affectée au marqueur inférieur pour y (lecture seule).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>y-axis value in plot coordinates assigned to the upper-y marker (read only).</source>
        <translation>Valeur de l&apos;axe-y dans le graphique affectée au marqueur supérieur pour y (lecture seule).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Check when x-axis on plot has log scale.</source>
        <translation>Échelle logarithmique sur l&apos;axe des x.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Check when y-axis on plot has log scale.</source>
        <translation>Échelle logarithmique sur l&apos;axe des y.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Image Info and Adjustment:
Window displaying info on the current image and controls
that allows one to scale (zoom in/out) and rotate the plot.</source>
        <translation>Info et réglage de l&apos;image :
Fenêtre affichant des informations sur l&apos;image actuelle et des contrôles
qui permettent de mettre à l&apos;échelle (zoom avant/arrière) et de faire pivoter le tracé.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Rotate current image (degrees).</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scale (zoom in/out) the current image.</source>
        <translation>Zoom avant/arrière sur le graphe.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show dimension and depth of current image.</source>
        <translation>Affiche la taille du graphe et la profondeur des couleurs.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error Bar Settings:
Define how to scan error bars.</source>
        <translation>Fenêtre de réglage des erreurs:
Y définir comment scanner les erreurs.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Data Table Window:
Window displaying the data table that holds 
 all points (and error bars) scanned so far.</source>
        <translation>Fenêtre de tableau de valeurs:
Affiche le tableau des données qui contient
tous les points (et les erreurs) scannés jusque-là.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data table holding all points scanned so far.</source>
        <translation>Tableau de valeurs contenant les points déjà scannés.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open file to read in image.</source>
        <translation>Ouvrir un fichier pour lire une image.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scan screen directly without loading image.
xyscan will turn semi-transparent.</source>
        <translation>Scanner l&apos;écran directement sans charger d&apos;image.
xyscan deviendra semi-transparent.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save the scanned data in text file.</source>
        <translation>Enregistrer les données acquises dans un fichier texte.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print the plot together with the scanned data.</source>
        <translation>Imprimer le graphe et les données scannées.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit xyscan.</source>
        <translation>Quitter xyscan.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete last scanned point from data table.</source>
        <translation>Effacer le dernier point du tableau de valeurs.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete all scanned point from data table.</source>
        <translation>Effacer tous les points du tableau de valeurs.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write comment that will be added to the
scanned data when saved to file.</source>
        <translation>Écrire un commentaire qui sera ajouté aux données
numérisées lorsqu&apos;elles seront enregistrées dans le fichier.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste image from clipboard.</source>
        <translation>Coller l&apos;image depuis le presse-papier.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear list of recently opened files.</source>
        <translation>Effacer la liste des fichiers récemment ouverts.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shows scan precision at current cursor point.</source>
        <translation>Affiche la précision du point sélectionné.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch on/off tool tips.</source>
        <translation>(Dés)activer les bulles d&apos;aide.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cursor Zoom</source>
        <translation>Zoom du curseur</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Cursor Zoom</source>
        <translation>Zoom du &amp;Curseur</translation>
    </message>
</context>
<context>
    <name>xyscanDataTable</name>
    <message>
        <location filename="../src/xyscanDataTable.cpp" line="+56"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Shift+F</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>-dx</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>+dx</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>-dy</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>+dy</source>
        <translation></translation>
    </message>
    <message>
        <location line="+195"/>
        <source>Scanned by: %1
</source>
        <translation>Scanné par: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment: %1
</source>
        <translation>Commentaire: %1
</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>// Scanned by: </source>
        <translation>// Scanné par: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>// Comment: </source>
        <translation>// Commentaire: </translation>
    </message>
    <message>
        <location line="+190"/>
        <source># Scanned by: </source>
        <translation># Scanné par: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source># Comment: </source>
        <translation># Commentaire: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Format: x, y</source>
        <translation></translation>
    </message>
    <message>
        <location line="-275"/>
        <source>xyscan Version %1
</source>
        <translation>Version d&apos;xyscan %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date: %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Source: %1
</source>
        <translation></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>// ROOT macro: </source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source> (autogenerated by xyscan)</source>
        <translation> (autogénéré par xyscan)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// xyscan Version </source>
        <translation>// Version d&apos;xyscan </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// Date: </source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>// Source: </source>
        <translation></translation>
    </message>
    <message>
        <location line="+189"/>
        <source># xyscan Version </source>
        <translation># Version d&apos;xyscan </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Date: </source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source># Source: </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>xyscanErrorBarScanModeToolBox</name>
    <message>
        <location filename="../src/xyscanErrorBarScanModeToolBox.cpp" line="+100"/>
        <location line="+7"/>
        <source>No Scan</source>
        <translation>Aucune scan</translation>
    </message>
    <message>
        <location line="-6"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Asymmetric</source>
        <translation>Asymétriques</translation>
    </message>
    <message>
        <location line="-58"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Symmetric (mean)</source>
        <translation>Symétriques (moyenne)</translation>
    </message>
    <message>
        <location line="-58"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Symmetric (max)</source>
        <translation>Symétriques (max)</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Defines scan mode for error bars in x.</source>
        <translation>Définit le mode de scan d&apos;erreur pour les erreurs en x.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Defines scan mode for error bars in y.</source>
        <translation>Définit le mode de scan d&apos;erreur pour les erreurs en y.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add additional x error bar scans.</source>
        <translation>Ajoutez des scans de barre d&apos;erreur en x supplémentaires.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add additional y error bar scans.</source>
        <translation>Ajoutez des scans de barre d&apos;erreur en y supplémentaires.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove additional x error bar scans.</source>
        <translation>Supprimez les scans supplémentaires de la barre d&apos;erreur en x.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove additional y error bar scans.</source>
        <translation>Supprimez les scans supplémentaires de la barre d&apos;erreur en y.</translation>
    </message>
</context>
<context>
    <name>xyscanHelpBrowser</name>
    <message>
        <location filename="../src/xyscanHelpBrowser.cpp" line="+27"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Cannot open the index descriptor file to setup the documentation. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Le fichier d&apos;index de la documentation est introuvable. Aucune aide ne sera disponible. Verifier votre installation et re-installer si necessaire.</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Help: %1</source>
        <translation>Aide: %1</translation>
    </message>
</context>
<context>
    <name>xyscanScanTasksHandler</name>
    <message>
        <location filename="../src/xyscanScanTasksHandler.cpp" line="+174"/>
        <location line="+31"/>
        <source>Scan x-error (-dx): move crosshair to end of left error bar and press [space].</source>
        <translation>Erreur-x (-dx): Placez le curseur sur la limite gauche de la barre d&apos;erreur et appuyez sur la touche [espace].</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+34"/>
        <source>Scan y-error (-dy): move crosshair to end of lower error bar and press [space].</source>
        <translation>Erreur-y (-dy):  Placez le curseur sur la limite inférieure de la barre d&apos;erreur et appuyez sur la touche [espace].</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Scan x-error (+dx): move crosshair to end of right error bar and press [space].</source>
        <translation>Erreur-x (+dx):  Placez le curseur sur la limite droite de la barre d&apos;erreur et appuyez sur la touche [espace].</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Scan y-error (+dy): move crosshair to end of upper error bar and press [space].</source>
        <translation>Erreur-y (+dy):  Placez le curseur sur la limite supérieure de la barre d&apos;erreur et appuyez sur la touche [espace].</translation>
    </message>
    <message>
        <location line="-79"/>
        <source>Ready to scan. Press space bar to record current cursor position.</source>
        <translation>Prêt pour l&apos;aquisition. Appuyez sur la barre d&apos;espace pour enregistrer le point sélectionné.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Data point and errors stored (%1).</source>
        <translation>Points de donnée et erreurs stockés (%1).</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Data point stored (%1).</source>
        <translation>Point de donnée sauvé (%1).</translation>
    </message>
</context>
<context>
    <name>xyscanUpdater</name>
    <message>
        <location filename="../src/xyscanUpdater.cpp" line="+51"/>
        <location line="+27"/>
        <source>xyscan</source>
        <translation></translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Cannot check for latest version.
(%1).

Make sure you are connected to a network and try again.</source>
        <translation>Impossible de vérifier la dernière version disponible.
(%1). 

Vérifiez votre connection au réseau et recommencez.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Parsing of xml file failed. Cannot check for newer version. Try again later.</source>
        <translation>EcL&apos;analyse du fichier xml a échouée. Impossible de vérifier les mises à jour. Réessayez plus tard.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;A new version of xyscan (%1) is available.&lt;br&gt;To download go to:&lt;br&gt;&lt;a href=&quot;%2&quot;&gt;%2&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Une nouvelle version d&apos;xyscan (%1) est disponible.&lt;br&gt;Téléchargez-la a partir de: &lt;br&gt;&lt;a href=&quot;%2&quot;&gt;%2&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are running the latest version of xyscan (%1).</source>
        <translation>Vous utilisez la dernière version d&apos;xyscan (%1).</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Strange, you are running a newer version (%1) than the latest version available on the web (%2).</source>
        <translation>C&apos;est étrange, vous utilisez une version (%1) plus récente que la dernière disponible sur Internet (%2).</translation>
    </message>
</context>
<context>
    <name>xyscanWindow</name>
    <message>
        <location filename="../src/xyscanWindow.cpp" line="+69"/>
        <source>Image file to scan</source>
        <translation>Fichier d&apos;image à scanner</translation>
    </message>
    <message>
        <source>No plot loaded</source>
        <translation type="vanished">Aucune figure chargée</translation>
    </message>
    <message>
        <source>Rotate:</source>
        <translation type="vanished">Rotation:</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation type="vanished">Echelle:</translation>
    </message>
    <message>
        <source>Lower x:</source>
        <translation type="vanished">x inférieur:</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">Définir</translation>
    </message>
    <message>
        <source>Scan area. The loaded graphics
is displayed here.</source>
        <translation type="vanished">Aire du scan. La figure que vous avez
chargée est visible ici.</translation>
    </message>
    <message>
        <source>Upper x:</source>
        <translation type="vanished">x supérieur:</translation>
    </message>
    <message>
        <source>Lower y:</source>
        <translation type="vanished">y inférieur:</translation>
    </message>
    <message>
        <source>Upper y:</source>
        <translation type="vanished">y supérieur:</translation>
    </message>
    <message>
        <source>Lin/Log Scale:</source>
        <translation type="vanished">Échelle Lin/Log:</translation>
    </message>
    <message>
        <source>Pixel:</source>
        <translation type="vanished">En pixel:</translation>
    </message>
    <message>
        <source>Plot Coordinates:</source>
        <translation type="vanished">A l&apos;échelle du graphe:</translation>
    </message>
    <message>
        <source>Cannot find the directory holding the help files (docs). No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="vanished">Le répertoire contenant la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <source>Cannot find the index descriptor to setup the help system. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="vanished">Le fichier d&apos;index de la documentation est introuvable. Aucune aide ne sera disponible. Verifiez votre installation et re-installer si nécessaire.</translation>
    </message>
    <message>
        <source>Density Histogram</source>
        <translation type="vanished">Histogramme de densité</translation>
    </message>
    <message>
        <source>&amp;Density Histogram</source>
        <translation type="vanished">Histogramme de &amp;densité</translation>
    </message>
    <message>
        <location line="+416"/>
        <source>Start Measuring</source>
        <translation>Commencer à mesurer</translation>
    </message>
    <message>
        <source>Units:</source>
        <translation type="vanished">Unité:</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="vanished">&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="vanished">&amp;Édition</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Aide</translation>
    </message>
    <message>
        <source>Crosshair Color...</source>
        <translation type="vanished">Couleur de la croix...</translation>
    </message>
    <message>
        <source>Delete &amp;Last</source>
        <translation type="vanished">Effacer &amp;le dernier point</translation>
    </message>
    <message>
        <source>Delete &amp;All</source>
        <translation type="vanished">Eff&amp;acer tous les points</translation>
    </message>
    <message>
        <source>&amp;Comment...</source>
        <translation type="vanished">&amp;Commentaires...</translation>
    </message>
    <message>
        <source>&amp;Current Precision</source>
        <translation type="vanished">Pré&amp;cision</translation>
    </message>
    <message>
        <source>Scan Screen</source>
        <translation type="vanished">Écran du scan</translation>
    </message>
    <message>
        <source>&amp;About xyscan</source>
        <translation type="vanished">&amp;A propos d&apos;xyscan</translation>
    </message>
    <message>
        <source>xyscan &amp;Help</source>
        <translation type="vanished">&amp;Aide xyscan (en anglais)</translation>
    </message>
    <message>
        <source>&amp;Tool Tips</source>
        <translation type="vanished">&amp;Bulles d&apos;aide</translation>
    </message>
    <message>
        <source>&amp;Check For Updates ...</source>
        <translation type="vanished">&amp;Vérifier les mises à jour...</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+42"/>
        <location line="+957"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Data Table</source>
        <translation type="vanished">Tableau de Valeurs</translation>
    </message>
    <message>
        <source>&amp;Data Table</source>
        <translation type="vanished">&amp;Tableau de Valeurs</translation>
    </message>
    <message>
        <source>Axis Settings</source>
        <translation type="vanished">Réglage des axes</translation>
    </message>
    <message>
        <source>Help and Documentation for xyscan.</source>
        <translation type="vanished">Aide et documentation pour xyscan.</translation>
    </message>
    <message>
        <source>Status bar. Displays instruction during scanning.</source>
        <translation type="vanished">Barre de statut. Affiche des instructions pendant le scan.</translation>
    </message>
    <message>
        <source>Cursor Zoom Window:
Displays a zoomed view of the area
surrounding the cross-hair allowing
for higher scan precision.</source>
        <translation type="vanished">Curseur de la fenêtre d&apos;agrandissement:
Affiche une vue agrandie de la région autour
du curseur pour une plus grande précision
du scan.</translation>
    </message>
    <message>
        <source>Axis Settings Window:
Use to set axes marker
and set log/lin scales.
Shortcut is %1+A.</source>
        <translation type="vanished">Fenêtre de réglage des axes:
À utiliser pour régler le maquage des axes
et choisir les échelles log/lin.
Le raccourci est %1+A.</translation>
    </message>
    <message>
        <source>Set value of marker for the lower x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+1.</source>
        <translation type="vanished">Entrer la valeur du marqueur pour la position
inférieure de l&apos;axe des abscisses x. Une boite de
dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du 
graphique. Le raccourci est %1+1.</translation>
    </message>
    <message>
        <source>Set value of marker for the upper x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+2.</source>
        <translation type="vanished">Entrer la valeur du marqueur pour la position
supérieure de l&apos;axe des abscisses x. Une boite
de dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du
graphique. Le raccourci est %1+2.</translation>
    </message>
    <message>
        <source>Set value of marker for the upper y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+4.</source>
        <translation type="vanished">Entrer la valeur du marqueur pour la position
supérieure de l&apos;axe des ordonnées y. Une boite
de dialogue apparait pour la saisie de la valeur
correspondante dans les coordonnées du
graphique. Le raccourci est %1+4.</translation>
    </message>
    <message>
        <source>Plot Info and Adjustment:
Window displaying info on the current plot and controls
that allows one to scale (zoom in/out) and rotate the plot.</source>
        <translation type="vanished">Fenêtre d&apos;ajustement:
Affiche des informations sur le graphe, permet d&apos;ajuster
le facteur de zoom et d&apos;effectuer une rotation sur le graphe.</translation>
    </message>
    <message>
        <source>Data table holding all points scanned so far.</source>
        <translation type="vanished">Tableau de valeurs contenant les points déjà scannés.</translation>
    </message>
    <message>
        <source>Cursor Zoom</source>
        <translation type="vanished">Zoom du curseur</translation>
    </message>
    <message>
        <source>&amp;Cursor Zoom</source>
        <translation type="vanished">Zoom du &amp;Curseur</translation>
    </message>
    <message>
        <location line="-1054"/>
        <source>Crosshair Color</source>
        <translation>Couleur de la croix</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Path Color</source>
        <translation>Couleur de la trajectoire</translation>
    </message>
    <message>
        <location line="+404"/>
        <source>Sorry no help available.
Help files are missing in this installation. Check your installation and reinstall if necessary.</source>
        <translation>Désolé, l&apos;aide n&apos;est pas disponible.
Les fichiers d&apos;aide sont absents de cette installation. Vérifiez l&apos;installation et réinstallez si nécessaire.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Image loaded, no markers set.</source>
        <translation>Image chargée, pas de marqueurs définis.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimension:&amp;nbsp;&lt;td&gt;%1 x %2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Depth:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha channel:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Device pixel ratio:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimensions:&amp;nbsp;&lt;td&gt;%1x%2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Profondeur:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Canal alpha:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rapport de pixels&amp;nbsp;&amp;nbsp;&lt;td&gt;&amp;nbsp;&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;de l&apos;appareil:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location line="+237"/>
        <source>On this platform pdf files are currently not supported. Please convert the file into one of the supported pixel formats and try again.</source>
        <translation>Seuls les fichiers pdf sont supportés sur cette plateforme. Veuillez convertir le fichier vers l&apos;un des formats de pixels supportés et réssayer.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot open PDF file &apos;%1&apos;.</source>
        <translation>Impossible d&apos;ouvrir le fichier PDF &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>PDF file &apos;%1&apos; is empty (no pages).</source>
        <translation>Le fichier PDF &apos;%1&apos; est vide (aucune page).</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select Page</source>
        <translation>Sélectionnez la page</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The PDF file has %1 pages.
Select the desired page:</source>
        <translation>Le fichier PDF comporte %1 pages.
Sélectionnez la page souhaitée :</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <source>PDF can be scaled before loading.

Enter scale:</source>
        <translation type="vanished">Le PDF peut être redimensionné avant le chargement

Entrez l&apos;échelle:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Cannot load PDF file &apos;%1&apos;.</source>
        <translation>Impossible de charger le fichier PDF « %1 ».</translation>
    </message>
    <message>
        <location line="+202"/>
        <source>Save File</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Text (*.txt);;Root Macro (*.C);;CSV (*.csv)</source>
        <translation>Texte (*.txt);;Macro Root (*.C);; CSV (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Impossible d&apos;enregistrer le fichier %1:
%2.</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Error in calculating transformation.
Markers on x-axis are less than two pixels apart. Cannot continue with current settings.</source>
        <translation>Erreur lors du calcul de l&apos;échelle.
Les marqueurs sur l&apos;axe x sont espacés de moins de deux pixels. Impossible de continuer.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error in calculating transformation.
Markers on y-axis are less than two pixels apart. Cannot continue with current settings.</source>
        <translation>Erreur lors du calcul de l&apos;échelle.
Les marqueurs sur l&apos;axe y sont espacés de moins de deux pixels. Impossible de continuer.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error in calculating transformation.
Logarithmic x-axis selected but negative (or zero) values assigned to markers. Cannot continue with current settings.</source>
        <translation>Erreur lors du calcul de l&apos;échelle.
+L&apos;échelle logarithmique sur l&apos;axe x ne peut être définie avec des valeurs négatives ou nulles. Impossible de continuer.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error in calculating transformation.
Logarithmic y-axis selected but negative (or zero) values assigned to markers. Cannot continue with current settings.</source>
        <translation>Erreur lors du calcul de l&apos;échelle.
+L&apos;échelle logarithmique sur l&apos;axe y ne peut être définie avec des valeurs négatives ou nulles. Impossible de continuer.</translation>
    </message>
    <message>
        <location line="-751"/>
        <location line="+846"/>
        <source>Ready to scan. Press space bar to record current cursor position.</source>
        <translation>Prêt pour l&apos;aquisition. Appuyez sur la barre d&apos;espace pour enregistrer le point sélectionné.</translation>
    </message>
    <message>
        <location line="-436"/>
        <source>PDF can be scaled before loading.
Original resolution is %1 x %2.
Enter scale:</source>
        <translation>Le PDF peut être mis à l&apos;échelle avant le chargement.
La résolution originale est de %1 x %2.
Entrez l&apos;échelle :</translation>
    </message>
    <message>
        <location line="+492"/>
        <source>Estimated precision at current point:
dx = +%1  -%2
dy = +%3  -%4</source>
        <translation>Précision estimée pour le point sélectionné:
dx = +%1  -%2
dy = +%3  -%4</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot determin precision yet.
Place all 4 markers first.</source>
        <translation>Impossible de déterminer la précision.
Placez d&apos;abord les 4 marqueurs.</translation>
    </message>
    <message>
        <location line="-1727"/>
        <source>xyscan, a data thief for scientific applications</source>
        <translation>xyscan, un voleur de données pour applications scientifiques</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Ctrl+Shift+C</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Shift+L</source>
        <translation></translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Do you want to save the data you scanned?
The content of the data table will be lost if you don’t save it.</source>
        <translation>Voulez-vous enregistrer les données que vous avez numérisées?
Le contenu de la table de données sera perdu si vous ne l&apos;enregistrez pas.</translation>
    </message>
    <message>
        <location line="+1737"/>
        <location line="+1"/>
        <source>N/A</source>
        <translatorcomment>ne s’applique pas . However the N/A should probably be changed to start off with. May be just blank?</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="vanished">Horizontal</translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="vanished">Vertical</translation>
    </message>
    <message>
        <source>Grayscale Densities Around Cursor</source>
        <translation type="vanished">Densité de niveaux de gris autour du curseur</translation>
    </message>
    <message>
        <source>Horizontal and Vertical </source>
        <translation type="vanished">Horizontal et vertical </translation>
    </message>
    <message>
        <source>Horizontal Only </source>
        <translation type="vanished">Horizontal seulement </translation>
    </message>
    <message>
        <source>Vertical Only </source>
        <translation type="vanished">Vertical seulement </translation>
    </message>
    <message>
        <source>Scale:</source>
        <comment>short</comment>
        <translation type="vanished">Echelle:</translation>
    </message>
    <message>
        <source>Pixel</source>
        <translation type="vanished">Pixel</translation>
    </message>
    <message>
        <source>Plot Units</source>
        <translatorcomment>For layout reasons this need to be kept as short as possible</translatorcomment>
        <translation type="vanished">Graphe</translation>
    </message>
    <message>
        <source>User Scale</source>
        <translation type="vanished">Personnalisée</translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation type="vanished">Ouvrir &amp;Récents</translation>
    </message>
    <message>
        <source>Significant Digits</source>
        <translation type="vanished">Chiffres significatifs</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="vanished">A&amp;ffichage</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation type="vanished">&amp;Ouvrir...</translation>
    </message>
    <message>
        <source>&amp;Clear History</source>
        <translation type="vanished">Effacer l&apos;&amp;historique</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation type="vanished">&amp;Enregistrer...</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="vanished">&amp;Imprimer...</translation>
    </message>
    <message>
        <source>&amp;Quit xyscan</source>
        <translation type="vanished">&amp;Quitter xyscan</translation>
    </message>
    <message>
        <source>&amp;Paste Image</source>
        <translation type="vanished">&amp;Coller une image</translation>
    </message>
    <message>
        <source>Marker Color...</source>
        <translation type="vanished">Couleur du marqueur...</translation>
    </message>
    <message>
        <source>Plot Info and Adjustment</source>
        <translation type="vanished">Ajustements et infos graphiques</translation>
    </message>
    <message>
        <source>Error Bar Scan Mode</source>
        <translatorcomment> Barres d&apos;erreurs</translatorcomment>
        <translation type="vanished">Mode de scan de barres d&apos;erreur</translation>
    </message>
    <message>
        <source>Coordinate Display</source>
        <translation type="vanished">Affichage des coordonnées</translation>
    </message>
    <message>
        <source>Measure Tool</source>
        <translation type="vanished">Outil de mesure</translation>
    </message>
    <message>
        <source>Horizontal bar of crosshairs cursor.</source>
        <translation type="vanished">Axe horizontal du curseur.</translation>
    </message>
    <message>
        <source>Vertical bar of crosshairs cursor.</source>
        <translation type="vanished">Axe vertical du curseur.</translation>
    </message>
    <message>
        <source>Marker for lower x-axis position.</source>
        <translation type="vanished">Marqueur pour la position inférieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <source>Marker for upper x-axis position.</source>
        <translation type="vanished">Marqueur pour la position supérieure de l&apos;axe des x.</translation>
    </message>
    <message>
        <source>Marker for lower y-axis position.</source>
        <translation type="vanished">Marqueur pour la position inférieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <source>Marker for upper y-axis position.</source>
        <translation type="vanished">Marqueur pour la position supérieure de l&apos;axe des y.</translation>
    </message>
    <message>
        <source>Measure Tool Window:
Allows to measure distances in plots and maps.</source>
        <translation type="vanished">Outil de mesure:
Permet de mesurer les distances dans les graphes et cartes.</translation>
    </message>
    <message>
        <source>Unit Selector:
Select the units in which the distance is displayed.
</source>
        <translation type="vanished">Choix de l&apos;unité:
Sélectionne l&apos;unité dans laquelle la distance est affichée.
</translation>
    </message>
    <message>
        <source>User Scale:
Allows to enter a scale in which the measured
distances will be expressed.</source>
        <translation type="vanished">Échelle personnalisée:
Permet d&apos;entrer une échelle dans laquelle
les distances mesurées seront affichées.</translation>
    </message>
    <message>
        <source>Starts and stops measuring.</source>
        <translation type="vanished">Commence et arrȇte de mesurer.</translation>
    </message>
    <message>
        <source>While measuring, displays the horizontal distance.</source>
        <translation type="vanished">Pendant la mesure, affiche la distance horizontale.</translation>
    </message>
    <message>
        <source>While measuring, displays the vertical distance.</source>
        <translation type="vanished">Pendant la mesure, affiche la distance verticale.</translation>
    </message>
    <message>
        <source>While measuring, displays the radial distance.</source>
        <translation type="vanished">Pendant la mesure, affiche la distance absolue.</translation>
    </message>
    <message>
        <source>While measuring, displays the angle w.r.t. the horizontal axis.</source>
        <translation type="vanished">Pendant la mesure, affiche l&apos;angle par rapport à l&apos;axe horizontal.</translation>
    </message>
    <message>
        <source>Select angle to be displayed in radians.</source>
        <translation type="vanished">Sélectionnez l&apos;angle à afficher en radians.</translation>
    </message>
    <message>
        <source>Set the zoom scale.</source>
        <translation type="vanished">Réglage de l&apos;échelle du zoom.</translation>
    </message>
    <message>
        <source>Coordinates Window:
Displays cursor position in local
(pixel) and plot coordinates.
Shortcut is %1+C.</source>
        <translation type="vanished">Fenêtre d&apos;affichage des coordonnées:
Coordonnées locales (en pixel)
et à l&apos;échelle du graphe.
Le raccourci est %1+C.</translation>
    </message>
    <message>
        <source>Displays the x coordinate of the cursor in pixel (screen) units.</source>
        <translation type="vanished">Affiche l&apos;abscisse x du curseur en pixel.</translation>
    </message>
    <message>
        <source>Displays the y coordinate of the cursor in pixel (screen) units.</source>
        <translation type="vanished">Affiche l&apos;ordonnée y du curseur en pixel.</translation>
    </message>
    <message>
        <source>Displays the x coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation type="vanished">Affiche l&apos;abscisse x du curseur avec l&apos;échelle définie.
Cette valeur est enregistrée dans le tableau de valeurs
en appuyant sur la touche espace.</translation>
    </message>
    <message>
        <source>Displays the y coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation type="vanished">Affiche l&apos;ordonnée y du curseur avec l&apos;échelle définie.
Cette valeur est enregistrée dans le tableau de valeurs
en appuyant sur la touche espace.</translation>
    </message>
    <message>
        <source>Set value of marker for the lower y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+3.</source>
        <translation type="vanished">Entrer la valeur du marqueur pour la position inférieure
de l&apos;axe des ordonnées y. Une boite de dialogue apparait
pour la saisie de la valeur correspondante dans les
coordonnées du graphique. Le raccourci est %1+3.</translation>
    </message>
    <message>
        <source>x-axis value in plot coordinates assigned to the low-x marker (read only).</source>
        <translation type="vanished">Valeur de l&apos;axe-x dans le graphique affectée au marqueur inférieur pour x (lecture seule).</translation>
    </message>
    <message>
        <source>x-axis value in plot coordinates assigned to the upper-x marker (read only).</source>
        <translation type="vanished">Valeur de l&apos;axe-x dans le graphique affectée au marqueur supérieur pour x (lecture seule).</translation>
    </message>
    <message>
        <source>y-axis value in plot coordinates assigned to the low-y marker (read only).</source>
        <translation type="vanished">Valeur de l&apos;axe-y dans le graphique affectée au marqueur inférieur pour y (lecture seule).</translation>
    </message>
    <message>
        <source>y-axis value in plot coordinates assigned to the upper-y marker (read only).</source>
        <translation type="vanished">Valeur de l&apos;axe-y dans le graphique affectée au marqueur supérieur pour y (lecture seule).</translation>
    </message>
    <message>
        <source>Rotate current plot (degrees).</source>
        <translation type="vanished">Rotation du graphe (degrés).</translation>
    </message>
    <message>
        <source>Scale (zoom in/out) the current plot.</source>
        <translation type="vanished">Zoom avant/arrière sur le graphe.</translation>
    </message>
    <message>
        <source>Show dimension and depth of current plot.</source>
        <translation type="vanished">Affiche la taille du graphe et la profondeur des couleurs.</translation>
    </message>
    <message>
        <source>Error Bar Settings:
Define how to scan error bars.</source>
        <translation type="vanished">Fenêtre de réglage des erreurs:
Y définir comment scanner les erreurs.</translation>
    </message>
    <message>
        <source>Data Table Window:
Window displaying the data table that holds 
 all points (and error bars) scanned so far.</source>
        <translatorcomment>Fenêtre de tableau de valeurs </translatorcomment>
        <translation type="vanished">Fenêtre de tableau de valeurs:
Affiche le tableau des données qui contient
tous les points (et les erreurs) scannés jusque-là.</translation>
    </message>
    <message>
        <source>Open file to read in image.</source>
        <translation type="vanished">Ouvrir un fichier pour lire une image.</translation>
    </message>
    <message>
        <source>Scan screen directly without loading image.
xyscan will turn semi-transparent.</source>
        <translation type="vanished">Scanner l&apos;écran directement sans charger d&apos;image.
xyscan deviendra semi-transparent.</translation>
    </message>
    <message>
        <source>Save the scanned data in text file.</source>
        <translation type="vanished">Enregistrer les données acquises dans un fichier texte.</translation>
    </message>
    <message>
        <source>Print the plot together with the scanned data.</source>
        <translation type="vanished">Imprimer le graphe et les données scannées.</translation>
    </message>
    <message>
        <source>Quit xyscan.</source>
        <translation type="vanished">Quitter xyscan.</translation>
    </message>
    <message>
        <source>Delete last scanned point from data table.</source>
        <translation type="vanished">Effacer le dernier point du tableau de valeurs.</translation>
    </message>
    <message>
        <source>Delete all scanned point from data table.</source>
        <translation type="vanished">Effacer tous les points du tableau de valeurs.</translation>
    </message>
    <message>
        <source>Write comment that will be added to the
scanned data when saved to file.</source>
        <translation type="vanished">Les commentaires seront ajoutés lors de l&apos;enregistrement des données acquises.</translation>
    </message>
    <message>
        <source>Paste image from clipboard.</source>
        <translatorcomment>Clipboard == press-papier????</translatorcomment>
        <translation type="vanished">Coller une image depuis le presse-papier.</translation>
    </message>
    <message>
        <source>Clear list of recently opened files.</source>
        <translation type="vanished">Effacer la liste des fichiers récemment ouverts.</translation>
    </message>
    <message>
        <source>Shows scan precision at current cursor point.</source>
        <translation type="vanished">Affiche la précision du point sélectionné.</translation>
    </message>
    <message>
        <source>Switch on/off tool tips.</source>
        <translation type="vanished">(Dés)activer les bulles d&apos;aide.</translation>
    </message>
    <message>
        <location line="-1601"/>
        <source>xyscan - Comment</source>
        <translation>xyscan - Commentaire</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The following comment will be written together with the data when saved to file or when printed:	</source>
        <translation>Le commentaire suivant sera affiché avec les données lors de l&apos;enregistrement ou l&apos;impression du fichier:	</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Marker Color</source>
        <translation>Couleur du marqueur</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Ready for screen scan. No markers set.</source>
        <translation>Prêt pour le scan d&apos;écran. Pas de marqueur défini.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Stop Measuring</source>
        <translation>Arrêter de mesurer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Measuring tool is on.</source>
        <translation>L&apos;outil de mesure est activé.</translation>
    </message>
    <message>
        <location line="+309"/>
        <location line="+811"/>
        <source>Cannot scan yet. Not sufficient information available to perform the coordinate transformation. You need to define 2 points on the x-axis (x1 &amp; x1) and 2 on the y-axis (y1 &amp; y2).</source>
        <translation>Informations insuffisantes pour commencer l&apos;aquisition. Vous devez définir 2 points sur l&apos;axe des x (x1 et x2) and 2 points sur l&apos;axe des y (y1 et y2).</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimension:&amp;nbsp;&lt;td&gt;%1x%2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Depth:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha channel:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Device pixel ratio:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;table border=&quot;0&quot;&gt;
&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;
&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimensions:&amp;nbsp;&lt;td&gt;%1x%2
&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Profondeur:&amp;nbsp;&lt;td&gt;%3 bit
&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Canal alpha:&amp;nbsp;&lt;td&gt;%4
&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Rapport de pixels&amp;nbsp;&amp;nbsp;&lt;td&gt;&amp;nbsp;
&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;de l&apos;appareil:&amp;nbsp;&lt;td&gt;%5
&lt;/table&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-526"/>
        <location line="+294"/>
        <source>&amp;%1  %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-159"/>
        <source>Images (</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>*.%1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location line="-999"/>
        <location line="+1019"/>
        <source>File &apos;%1&apos; does not exist.</source>
        <translation>Le fichier &apos;%1&apos;; n&apos;existe pas.</translation>
    </message>
    <message>
        <source>Histogram Window:
Displays histogram that indicate the
grayscale densities around the cursor
position.</source>
        <translation type="vanished">Fenêtre d&apos;histogramme:
Affiche l&apos;histogramme qui indique la
densités de niveaux de gris autour de
la position du curseur.</translation>
    </message>
    <message>
        <source>Selects which data to show in the histogram.</source>
        <translation type="vanished">Sélectionne les données à afficher dans l&apos;histogramme.</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Failed to extract page from PDF file &apos;%1&apos;.</source>
        <translation>Impossible d&apos;extraire la page du fichier PDF &apos; %1 &apos;.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Raster Precision</source>
        <translatorcomment>Précision de la trame?</translatorcomment>
        <translation>Précision de rasterisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF needs to be rastered to be displayed.
Enter pixel/inch:</source>
        <translation>PDF doit être tramé pur être affiché.
Entrer le pixel/inch:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error rendering PDF file &apos;%1&apos;.</source>
        <translation>Erreur du rendu du fichier PDF &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot load image from file &apos;%1&apos;. Either the file content is damaged or the image file format is not supported.</source>
        <translation>Impossible de charger l&apos;image du fichier &apos;%1&apos;. Soit le contenu du fichier est endommagé soit le format de cette image n&apos;est pas supporté.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot load image from clipboard.
Either the clipboard does not contain an image or it contains an image in an unsupported image format.</source>
        <translation>Impossible de coller l&apos;image depuis le presse-papier.
Le presse-papier ne contient pas d&apos;image ou le format n&apos;est pas reconnu.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>xyscan - Print</source>
        <translation>xyscan - Imprimer</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>Data saved as ROOT macro &apos;%1&apos;.</source>
        <translation>Données enregistrées dans la macro ROOT &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Data saved as csv file &apos;%1&apos;.</source>
        <translation>Données enregistrées dans le fichier csv &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Data saved in &apos;%1&apos;.</source>
        <translation>Données enregistrées dans &apos;%1&apos;.</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>lower</source>
        <translation>inférieure</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>upper</source>
        <translation>supérieure</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the %1 %2-value at marker position:</source>
        <translation>Entrer la valeur %1 %2 à la position du marqueur:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>%1 is not a valid floating point number.</source>
        <translation>%1 n&apos;est pas un nombre réel valide.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 of 4 markers set.</source>
        <translation>%1 marqueur(s) sur 4 défini(s).</translation>
    </message>
    <message>
        <source>Check when x-axis on plot has log scale.</source>
        <translation type="vanished">Échelle logarithmique sur l&apos;axe des x.</translation>
    </message>
    <message>
        <source>Check when y-axis on plot has log scale.</source>
        <translation type="vanished">Échelle logarithmique sur l&apos;axe des y.</translation>
    </message>
</context>
</TS>
