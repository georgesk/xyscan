<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>xyscanAbout</name>
    <message>
        <location filename="../src/xyscanAbout.cpp" line="+31"/>
        <source>Release </source>
        <translation></translation>
    </message>
    <message>
        <source>Copyright (C) 2002-2020 Thomas S. Ullrich

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of
the License, or any later version.

This program is distributed  in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</source>
        <translation type="vanished">Copyright (C) 2002-2020 Thomas S. Ullrich

Dieses Programm ist freie Software. Sie können es unter den
Bedingungen der GNU General Public License, wie von der
Free Software Foundation veröffentlicht, weitergeben und/oder
modifizieren, entweder gemäß Version 3 der Lizenz oder
(nach Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
daß es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE
GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE
oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
Details finden Sie in der GNU General Public License.

Sie sollten ein Exemplar der GNU General Public License 
zusammen mit diesem Programm erhalten haben.
Falls nicht, siehe &lt;http://www.gnu.org/licenses/&gt;.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Copyright (C) 2002-2021 Thomas S. Ullrich

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of
the License, or any later version.

This program is distributed  in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.</source>
        <translation>Copyright (C) 2002-2021 Thomas S. Ullrich

Dieses Programm ist freie Software. Sie können es unter den
Bedingungen der GNU General Public License, wie von der
Free Software Foundation veröffentlicht, weitergeben und/oder
modifizieren, entweder gemäß Version 3 der Lizenz oder
(nach Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
daß es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE
GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE
oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
Details finden Sie in der GNU General Public License.

Sie sollten ein Exemplar der GNU General Public License 
zusammen mit diesem Programm erhalten haben.
Falls nicht, siehe &lt;http://www.gnu.org/licenses/&gt;.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>&amp;Ok</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+62"/>
        <source>&amp;License</source>
        <translation>&amp;Lizenz</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>About xyscan</source>
        <translation>xyscan Info</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Tit&amp;le &amp;&amp; Version</source>
        <translation>Tite&amp;l &amp;&amp; Version</translation>
    </message>
</context>
<context>
    <name>xyscanBaseWindow</name>
    <message>
        <location filename="../src/xyscanBaseWindow.cpp" line="+151"/>
        <source>No image loaded</source>
        <translation>Keine Grafik geladen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Rotate:</source>
        <translation>Rotieren:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Scale:</source>
        <translation>Skalieren:</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Lower x:</source>
        <translation>Unteres x:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+3"/>
        <location line="+3"/>
        <location line="+3"/>
        <source>Set</source>
        <translation>Setzen</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Upper x:</source>
        <translation>Oberes x:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lower y:</source>
        <translation>Unteres y:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Upper y:</source>
        <translation>Oberes y:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Log x</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Log y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Lin/Log Scale:</source>
        <translation>Lin/Log Skala:</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+4"/>
        <location line="+23"/>
        <location line="+4"/>
        <location line="+295"/>
        <location line="+18"/>
        <location line="+18"/>
        <location line="+22"/>
        <location line="+27"/>
        <source>N/A</source>
        <translation>-</translation>
    </message>
    <message>
        <location line="-354"/>
        <source>Pixel:</source>
        <translation>Pixel:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+3"/>
        <source>x:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+3"/>
        <source>y:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Plot Coordinates:</source>
        <translation>Bildkoordinaten:</translation>
    </message>
    <message>
        <source>Cannot find the directory holding the help files (docs). No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="obsolete">Das Verzeichnis mit den Hilfedateien kann nicht gefunden werden. Kein Zugriff auf das Onlinehandbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <source>Cannot find the index descriptor to setup the help system. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="obsolete">Die Indexdatei zum Erstellen der Dokumentation kann nicht geöffnet werden. Kein Zugriff auf das Onlinehandbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Cannot find the directory holding the help files. No help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Das Verzeichnis mit den Hilfedateien kann nicht gefunden werden. Kein Zugriff auf das Handbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot find the index descriptor to setup the help system. No help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Die Indexdatei zum Erstellen der Dokumentation kann nicht geöffnet werden. Kein Zugriff auf das Handbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Density Histogram</source>
        <translation>Histogramm der Graustufendichten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Horizontal</source>
        <translation>Horizontale</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Vertical</source>
        <translation>Vertikale</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Grayscale Densities Around Cursor</source>
        <translation>Graustufendichte entlang der Fadenkreuzachsen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Horizontal and Vertical </source>
        <translation>Horizontale und Vertikale </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Horizontal Only </source>
        <translation>Nur Horizontale </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Vertical Only </source>
        <translation>Nur Vertikale </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Density Histogram</source>
        <translation>Histogra&amp;mm</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Start Measuring</source>
        <translation>Messung starten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>dx:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>dy:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>r:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Radians</source>
        <translation>Radiant</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>L:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Reset</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Units:</source>
        <translation>Einheiten:</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scale:</source>
        <comment>short</comment>
        <translation>Skala:</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pixel</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plot Units</source>
        <translation>Grafik</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Scale</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Clear History</source>
        <translation>&amp;Verlauf Löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Save...</source>
        <translation>&amp;Speichern...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Quit xyscan</source>
        <translation>xyscan &amp;beenden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Paste Image</source>
        <translation>&amp;Grafik einfügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Crosshair Color...</source>
        <translation>Fadenkreuzfarbe...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Marker Color...</source>
        <translation>Markerfarbe...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Path Color...</source>
        <translation>Pfadfarbe...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Delete &amp;Last Point</source>
        <translation>&amp;Letzten Eintrag löschen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear Table</source>
        <translation>Tabelle löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Comment...</source>
        <translation>&amp;Kommentar...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Current Precision</source>
        <translation>Aktuelle &amp;Präzision</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scan Screen</source>
        <translation>Bildschirm scannen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About xyscan</source>
        <translation>xyscan &amp;Info</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>xyscan &amp;Help</source>
        <translation>xyscan &amp;Hilfe (in Englisch)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Tool Tips</source>
        <translation>&amp;Tooltips</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Check For Updates ...</source>
        <translation>Nach neuster &amp;Version suchen ...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;Recent</source>
        <translation>&amp;Zuletzt Verwendete öffnen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Significant Digits</source>
        <translation>Signifikante Stellen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Data Table</source>
        <translation>Datentabelle</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Data Table</source>
        <translation>&amp;Datentabelle</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Image Info and Adjustment</source>
        <translation>Grafik Info und Einrichtung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Axis Settings</source>
        <translation>Achsen definieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error Bar Scan Mode</source>
        <translation>Fehlerbalkenmodus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Coordinate Display</source>
        <translation>Koordinatenanzeige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Measure Tool</source>
        <translation>Messwerkzeug</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Ctrl</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Scan area. The loaded graphics
is displayed here.</source>
        <translation>Scan-Fenster. Die geladenen 
Grafiken werden hier angezeigt.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Help and Documentation for xyscan.</source>
        <translation>Hilfe und Dokumentation für xyscan.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Status bar. Displays instruction during scanning.</source>
        <translation>Statusleiste. Wichtige Instruktionen werden hier angezeigt.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Horizontal bar of crosshairs cursor.</source>
        <translation>Horizontale Linie des Fadenkreuzes.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Vertical bar of crosshairs cursor.</source>
        <translation>Vertikale Linie des Fadenkreuzes.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for lower x-axis position.</source>
        <translation>Marker für die kleinere (untere) x-Achsen Position.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for upper x-axis position.</source>
        <translation>Marker für die größere (obere) x-Achsen Position.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for lower y-axis position.</source>
        <translation>Marker für die kleinere (untere) y-Achsen Position.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Marker for upper y-axis position.</source>
        <translation>Marker für die größere (obere) y-Achsen Position.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Measure Tool Window:
Allows to measure distances and paths in plots and maps.</source>
        <translation>Messwerkzeug Rahmen:
Erlaubt Abstände, Entfernungen und Pfade 
in Grafiken und Karten zu messen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Starts and stops measuring.</source>
        <translation>Abstandsmessungen starten und beenden.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>If checked, will measure along a path (polyline)
instead measuring a point-to-point distance.
The points on the path are set by double-clicking
the mouse.</source>
        <translation>Wenn diese Option aktiviert ist, wird entlang eines Pfads (Polylinie) gemessen
und nicht wie ueblich nur Punkt-zu-Punkt.
Die Punkte auf dem Pfad werden durch Doppelklicken mit der Maus festgelegt.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>While measuring, displays the horizontal distance (dx).</source>
        <translation>Zeigt den horizontalen Abstand (dx) an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the vertical distance (dy).</source>
        <translation>Zeigt den vertikalen Abstand (dy) an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the radial distance (dr).</source>
        <translation>Zeigt den radialen Abstand (dr) an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>While measuring, displays the angle w.r.t. the horizontal axis.</source>
        <translation>Zeigt den Winkel des gewählten Punktes zur horizontalen Achse an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select if angles should be displayed in radians
instead of degrees.</source>
        <translation>Winkel in Radiant oder Grad anzeigen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unit Selector:
Select the units in which the distance is displayed.</source>
        <translation>Einstellung der Einheiten:
Hier werden die Einheiten festgelegt in denen
Abstände und Entfernungen angezeigt werden.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User Scale:
Allows to enter a scale in which the measured
distances will be expressed. The shown distances
are calculated by multiplying this scale by the
measured length in pixel. If you have an object in
the plot of which you know the length in your desired
units (e.g., cm, miles) you can measure it using the
measure tool (let it sit at the right distance) and
then enter the length of that object in its own units
and press Shift+Enter. This will then automatically
calculate, set, and display the correct scale.</source>
        <translation>Benutzer definierter Maßstab:
Erlaubt die Eingabe einer spezifischen Skala mit der die gemessene 
Entfernung angezeigt werden. Der angezeigte Abstand ist das Produkt
dieser Skala und der gemessenen Länge in Pixel. Wenn sie ein Objekt
in ihrer Grafik haben von dem sie Länge kennen, können sie das Objekt
mit dem Werkzeug messen (im richtigen Abstand sitzen lassen), dann
die Länge dieses Objekts in seinen eigenen Einheiten eingeben und 
dann Shift+Enter (Umschalten+Eingabe) drücken. Das wird dann 
automatisch für weitere Messungen in der Grafik den richtigen Maßstab 
berechnen, einstellen und anzeigen. Die Grafik ist nun geeicht.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Shows the measured path length.</source>
        <translation>Anzeige der gemessenen Pfadlänge.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resets the path length and deletes the path
superimposed on the image.</source>
        <translation>Setzt die Pfadlänge zurück auf 0 und löscht 
den angezeigten Pfad.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cursor Zoom Window:
Displays a zoomed view of the area
surrounding the cross-hair allowing
for higher scan precision.</source>
        <translation>Cursor-Zoom-Fenster:
Zeigt eine vergrößerte Ansicht des Bereichs um
das Fadenkreuz an. Kann unter Umständen
die Präzision des Scans verbessern.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set the zoom scale.</source>
        <translation>Den Vergrößerungsfaktor einstellen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Histogram Window:
Displays a histogram that indicates the
grayscale densities around the cursor
position.</source>
        <translation>Histogramm-Fenster:
Zeigt ein Histogramm an, das die
Graustufendichten um den Cursor
herum anzeigt.</translation>
    </message>
    <message>
        <source>Histogram Window:
Displays histogram that indicate the
grayscale densities around the cursor
position.</source>
        <translation type="obsolete">Histogramm Fenster:
Zeigt ein Histogramm welches die
Graustufendichte der Pixel entlang
der Fadenkreuzachsen widerspiegelt.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Selects which data to show in the histogram.</source>
        <translation>Definiert welche Daten im Histogramm 
dargestellt werden sollen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Coordinates Window:
Displays cursor position in local
(pixel) and plot coordinates.
Shortcut is %1+C.</source>
        <translation>Koordinatenfenster:
Zeigt die Cursorposition in lokalen
(Pixel) und Bild-Koordinaten an.
Die Abkürzung ist %1+C.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Displays the x coordinate of the cursor in pixel (screen) units.</source>
        <translation>Zeigt die x-Koordinate des Cursors in Pixel (Bildschirm) -Einheiten an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Displays the y coordinate of the cursor in pixel (screen) units.</source>
        <translation>Zeigt die y-Koordinate des Cursors in Pixel (Bildschirm) -Einheiten an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Displays the x coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation>Zeigt die x-Koordinate des Cursors in Bild-Einheiten an.
Wenn der Punkt aufgezeichnet wird (Leertaste), wird diese Koordinate in
der Datentabelle gespeichert.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Displays the y coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation>Zeigt die y-Koordinate des Cursors in Bild-Einheiten an.
Wenn der Punkt aufgezeichnet wird (Leertaste), wird diese Koordinate in
der Datentabelle gespeichert.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Axis Settings Window:
Use to set axes marker
and set log/lin scales.
Shortcut is %1+A.</source>
        <translation>Definition der Achsen:
Hier werden die Werte der gesetzten Marker
eingegeben und festgelegt ob die Achsen linear
oder logarithmisch sind.
Abkürzung ist %1+A.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set value of marker for the lower x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+1.</source>
        <translation>Setzt den Wert des Markers für die untere (kleineren) Position auf der x-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the upper x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+2.</source>
        <translation>Setzt den Wert des Markers für die obere (größere) Position auf der x-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+2.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the lower y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+3.</source>
        <translation>Setzt den Wert des Markers für die untere (kleinere) Position auf der y-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+3.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set value of marker for the upper y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+4.</source>
        <translation>Setzt den Wert des Markers für die oberen (größere) Position auf der y-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+4.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>x-axis value in plot coordinates assigned to the low-x marker (read only).</source>
        <translation>Momentaner Wert des unteren (kleineren) Markers auf der x-Achse.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>x-axis value in plot coordinates assigned to the upper-x marker (read only).</source>
        <translation>Momentaner Wert des oberen (größeren) Markers auf der x-Achse.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>y-axis value in plot coordinates assigned to the low-y marker (read only).</source>
        <translation>Momentaner Wert des unteren (kleineren) Markers auf der y-Achse.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>y-axis value in plot coordinates assigned to the upper-y marker (read only).</source>
        <translation>Momentaner Wert des oberen (größeren) Markers auf der y-Achse.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Check when x-axis on plot has log scale.</source>
        <translation>Angeben ob die x-Achse eine logarithmische Skala hat.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Check when y-axis on plot has log scale.</source>
        <translation>Angeben ob die y-Achse eine logarithmische Skala hat.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Image Info and Adjustment:
Window displaying info on the current image and controls
that allows one to scale (zoom in/out) and rotate the plot.</source>
        <translation>Grafik Info und Einrichtung:
Hier werden Informationen über die momentane Grafik angezeigt und
das Bild kann rotiert und vergrößert/verkleinert werden.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Rotate current image (degrees).</source>
        <translation>Hier kann man die Grafik rotieren (in Grad).</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scale (zoom in/out) the current image.</source>
        <translation>Hier kann die Grafik verkleinert und vergrößert werden.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show dimension and depth of current image.</source>
        <translation>Zeigt die Dimension und Bit-Tiefe der Grafik an.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error Bar Settings:
Define how to scan error bars.</source>
        <translation>Einstellung des Fehlerbalkenmodus:
Hier wird festgelegt ob und wie die Fehlerbalken eingescannt werden.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Data Table Window:
Window displaying the data table that holds 
 all points (and error bars) scanned so far.</source>
        <translation>Datentabelle:
Dieses Fenster zeigt eine Tabelle mit allen
bisher eingescannten Datenpunkten.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data table holding all points scanned so far.</source>
        <translation>Datentabelle mit allen eingescannten Punkten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open file to read in image.</source>
        <translation>Datei die Grafik enthält öffnen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scan screen directly without loading image.
xyscan will turn semi-transparent.</source>
        <translation>Grafiken direkt vom Bildschirm einscannen.
Das xyscan Fenster wird dafür transparent.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save the scanned data in text file.</source>
        <translation>Die gescannten Daten in einer Textdatei abspeichern.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print the plot together with the scanned data.</source>
        <translation>Die gescannten Daten zusammen mit der Grafik ausdrucken.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit xyscan.</source>
        <translation>xyscan beenden.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete last scanned point from data table.</source>
        <translation>Letzten Punkt (mit Fehlern) löschen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete all scanned point from data table.</source>
        <translation>Alle gespeicherten Punkte einschließlich Fehlern löschen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write comment that will be added to the
scanned data when saved to file.</source>
        <translation>Hier Kommentare eingegeben die zusammen
mit den Daten abgespeichert werden.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paste image from clipboard.</source>
        <translation>Grafik aus der Zwischenablage einfügen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clear list of recently opened files.</source>
        <translation>Liste der zuletzt geöffneten Dateien löschen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shows scan precision at current cursor point.</source>
        <translation>Zeigt die ungefähre Präzision am momentanen Punkt an.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Switch on/off tool tips.</source>
        <translation>Tooltips an und abschalten.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cursor Zoom</source>
        <translation>Cursor-Zoom</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Cursor Zoom</source>
        <translation>&amp;Cursor-Zoom</translation>
    </message>
</context>
<context>
    <name>xyscanDataTable</name>
    <message>
        <location filename="../src/xyscanDataTable.cpp" line="+56"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Shift+F</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>-dx</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>+dx</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>-dy</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>+dy</source>
        <translation></translation>
    </message>
    <message>
        <location line="+193"/>
        <source>xyscan Version %1
</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date: %1</source>
        <translation>Datum: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scanned by: %1
</source>
        <translation>Eingescannt von: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Source: %1
</source>
        <translation>Quelle: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comment: %1
</source>
        <translation>Kommentar: %1
</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>// ROOT macro: </source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source> (autogenerated by xyscan)</source>
        <translation> (von xyscan automatisch generiert)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// xyscan Version </source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// Date: </source>
        <translation>// Datum: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// Scanned by: </source>
        <translation>// Eingescannt von: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// Source: </source>
        <translation>// Quelle: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>// Comment: </source>
        <translation>// Kommentar: </translation>
    </message>
    <message>
        <location line="+188"/>
        <source># xyscan Version </source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Date: </source>
        <translation># Datum: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Scanned by: </source>
        <translation># Eingescannt von: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Source: </source>
        <translation># Quelle: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Comment: </source>
        <translation># Kommentar: </translation>
    </message>
    <message>
        <location line="+1"/>
        <source># Format: x, y</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>xyscanErrorBarScanModeToolBox</name>
    <message>
        <location filename="../src/xyscanErrorBarScanModeToolBox.cpp" line="+100"/>
        <location line="+7"/>
        <source>No Scan</source>
        <translation>Kein Scan</translation>
    </message>
    <message>
        <location line="-6"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Asymmetric</source>
        <translation>Asymmetrisch</translation>
    </message>
    <message>
        <location line="-58"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Symmetric (mean)</source>
        <translation>Symmetrisch (Mittel)</translation>
    </message>
    <message>
        <location line="-58"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+14"/>
        <source>Symmetric (max)</source>
        <translation>Symmetrisch (Max)</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Defines scan mode for error bars in x.</source>
        <translation>Den Scanmodus der Fehlerbalken in x definieren.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Defines scan mode for error bars in y.</source>
        <translation>Den Scanmodus der Fehlerbalken in y definieren.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add additional x error bar scans.</source>
        <translation>Fügen Sie zusätzliche x Fehlerbalkenscans hinzu.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add additional y error bar scans.</source>
        <translation>Fügen Sie zusätzliche y Fehlerbalkenscans hinzu.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove additional x error bar scans.</source>
        <translation>Entfernen von zusätzlichen x Fehlerbalkenscans.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove additional y error bar scans.</source>
        <translation>Entfernen von zusätzliche y Fehlerbalkenscans.</translation>
    </message>
</context>
<context>
    <name>xyscanHelpBrowser</name>
    <message>
        <location filename="../src/xyscanHelpBrowser.cpp" line="+27"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Home</source>
        <translation>Startseite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Cannot open the index descriptor file to setup the documentation. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation>Die Indexdatei zum Erstellen der Dokumentation kann nicht geöffnet werden. Der Zugriff auf das Onlinehandbuch wird nicht möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Help: %1</source>
        <translation>Hilfe: %1</translation>
    </message>
</context>
<context>
    <name>xyscanScanTasksHandler</name>
    <message>
        <location filename="../src/xyscanScanTasksHandler.cpp" line="+174"/>
        <location line="+31"/>
        <source>Scan x-error (-dx): move crosshair to end of left error bar and press [space].</source>
        <translation>Fehler in x scannen (-dx): Bewegen Sie das Fadenkreuz zum Ende der linken Fehlerleiste und drücken Sie [Leertaste].</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+34"/>
        <source>Scan y-error (-dy): move crosshair to end of lower error bar and press [space].</source>
        <translation>Fehler in y scannen (-dy): Bewegen Sie das Fadenkreuz zum Ende der unteren Fehlerleiste und drücken Sie [Leertaste].</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Scan x-error (+dx): move crosshair to end of right error bar and press [space].</source>
        <translation>Fehler in x scannen (+dx): Bewegen Sie das Fadenkreuz zum Ende der rechten Fehlerleiste und drücken Sie [Leertaste].</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Scan y-error (+dy): move crosshair to end of upper error bar and press [space].</source>
        <translation>Fehler in y scannen (+dy): Bewegen Sie das Fadenkreuz zum Ende der oberen Fehlerleiste und drücken Sie [Leertaste].</translation>
    </message>
    <message>
        <location line="-79"/>
        <source>Ready to scan. Press space bar to record current cursor position.</source>
        <translation>Bereit zum Scannen. Drücken Sie die Leertaste, um die aktuelle Cursorposition aufzuzeichnen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Data point and errors stored (%1).</source>
        <translation>Datenpunkt und Fehler wurden abgespeichert (%1).</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Data point stored (%1).</source>
        <translation>Der Datenpunkt wurde abgespeichert (%1).</translation>
    </message>
</context>
<context>
    <name>xyscanUpdater</name>
    <message>
        <location filename="../src/xyscanUpdater.cpp" line="+51"/>
        <location line="+27"/>
        <source>xyscan</source>
        <translation></translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Cannot check for latest version.
(%1).

Make sure you are connected to a network and try again.</source>
        <translation>Nach der neuesten Version kann leider nicht gesucht werden.
(%1).

Stellen Sie sicher, dass sie mit einem Netzwerk
verbunden sind und versuchen Sie es nochmal.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Parsing of xml file failed. Cannot check for newer version. Try again later.</source>
        <translation>Die Analyse der XML-Datei ist fehlgeschlagen. Nach einer neueren Version kann leider nicht gesucht werden. Versuchen sie es später noch einmal.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;A new version of xyscan (%1) is available.&lt;br&gt;To download go to:&lt;br&gt;&lt;a href=&quot;%2&quot;&gt;%2&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Eine neue Version von xyscan (%1) ist verfügbar.&lt;br&gt;Zum Herunterladen gehen Sie zu:&lt;br&gt;&lt;a href=&quot;%2&quot;&gt;%2&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are running the latest version of xyscan (%1).</source>
        <translation>Sie haben die neueste Version von xyscan (%1).</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Strange, you are running a newer version (%1) than the latest version available on the web (%2).</source>
        <translation>Seltsam! Sie haben eine Version (%1) die neuer ist als die neueste Version auf dem Web (%2).</translation>
    </message>
</context>
<context>
    <name>xyscanWindow</name>
    <message>
        <location filename="../src/xyscanWindow.cpp" line="+66"/>
        <source>xyscan, a data thief for scientific applications</source>
        <translation>xyscan, ein Datendieb für wissenschaftliche Anwendungen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Image file to scan</source>
        <translation>Zu scannende Grafikdatei</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Do you want to save the data you scanned?
The content of the data table will be lost if you don’t save it.</source>
        <translation>Möchten sie die gescannten Daten abspeichern?
Der Inhalt der Datentabelle geht ansonsten verloren.</translation>
    </message>
    <message>
        <source>No plot loaded</source>
        <translation type="vanished">Keine Grafik geladen</translation>
    </message>
    <message>
        <source>Rotate:</source>
        <translation type="vanished">Rotieren:</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation type="vanished">Skalieren:</translation>
    </message>
    <message>
        <source>Lower x:</source>
        <translation type="vanished">Unteres x:</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">Setzen</translation>
    </message>
    <message>
        <source>Density Histogram</source>
        <translation type="vanished">Histogramm der Graustufendichten</translation>
    </message>
    <message>
        <source>&amp;Density Histogram</source>
        <translation type="vanished">Histogra&amp;mm</translation>
    </message>
    <message>
        <source>Units:</source>
        <translation type="vanished">Einheiten:</translation>
    </message>
    <message>
        <source>Coordinate Display</source>
        <translation type="vanished">Koordinatenanzeige</translation>
    </message>
    <message>
        <source>Cursor Zoom Window:
Displays a zoomed view of the area
surrounding the cross-hair allowing
for higher scan precision.</source>
        <translation type="vanished">Cursor-Zoom-Fenster:
Zeigt eine vergrößerte Ansicht des Bereichs um
das Fadenkreuz an. Kann unter Umständen
die Präzision des Scans verbessern.</translation>
    </message>
    <message>
        <source>Histogram Window:
Displays histogram that indicate the
grayscale densities around the cursor
position.</source>
        <translation type="vanished">Histogramm Fenster:
Zeigt ein Histogramm welches die
Graustufendichte der Pixel entlang
der Fadenkreuzachsen widerspiegelt.</translation>
    </message>
    <message>
        <source>Selects which data to show in the histogram.</source>
        <translation type="vanished">Definiert welche Daten im Histogramm 
dargestellt werden sollen.</translation>
    </message>
    <message>
        <source>Set value of marker for the lower x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+1.</source>
        <translation type="vanished">Setzt den Wert des Markers für die untere (kleineren) Position auf der x-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+1.</translation>
    </message>
    <message>
        <source>Set value of marker for the upper x-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+2.</source>
        <translation type="vanished">Setzt den Wert des Markers für die obere (größere) Position auf der x-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+2.</translation>
    </message>
    <message>
        <source>Set value of marker for the upper y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+4.</source>
        <translation type="vanished">Setzt den Wert des Markers für die oberen (größere) Position auf der y-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+4.</translation>
    </message>
    <message>
        <source>Data table holding all points scanned so far.</source>
        <translation type="vanished">Datentabelle mit allen eingescannten Punkten.</translation>
    </message>
    <message>
        <location line="+1010"/>
        <source>On this platform pdf files are currently not supported. Please convert the file into one of the supported pixel formats and try again.</source>
        <translation>Auf dieser Plattform ist das Einlesen von PDF Dateien leider nicht unterstützt. Bitte die Grafik in ein erlaubtes Format konvertieren.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Scale</source>
        <translation>Skalierung</translation>
    </message>
    <message>
        <source>PDF can be scaled before loading.

Enter scale:</source>
        <translation type="vanished">Die PDF-Grafik kann vor dem Einladen skaliert werden.

Skalierungsfaktor:</translation>
    </message>
    <message>
        <source>Upper x:</source>
        <translation type="vanished">Oberes x:</translation>
    </message>
    <message>
        <location line="-1178"/>
        <source>Ctrl+Shift+C</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Shift+L</source>
        <translation></translation>
    </message>
    <message>
        <source>Lower y:</source>
        <translation type="vanished">Unteres y:</translation>
    </message>
    <message>
        <source>Upper y:</source>
        <translation type="vanished">Oberes y:</translation>
    </message>
    <message>
        <source>Lin/Log Scale:</source>
        <translation type="vanished">Lin/Log Skala:</translation>
    </message>
    <message>
        <location line="+1865"/>
        <location line="+1"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <source>Pixel:</source>
        <translation type="vanished">Pixel:</translation>
    </message>
    <message>
        <source>Plot Coordinates:</source>
        <translation type="vanished">Bildkoordinaten:</translation>
    </message>
    <message>
        <source>Cannot find the directory holding the help files (docs). No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="vanished">Das Verzeichnis mit den Hilfedateien (docs) kann nicht gefunden werden. Kein Zugriff auf das Onlinehandbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <source>Cannot find the index descriptor to setup the help system. No online help will be available. Check your installation and reinstall if necessary.</source>
        <translation type="vanished">Die Indexdatei zum Erstellen der Dokumentation kann nicht geöffnet werden. Kein Zugriff auf das Onlinehandbuch wird möglich sein. Versuchen sie bitte xyscan neu zu installieren.</translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="vanished">Horizontale</translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="vanished">Vertikale</translation>
    </message>
    <message>
        <source>Grayscale Densities Around Cursor</source>
        <translation type="vanished">Graustufendichte entlang der Fadenkreuzachsen</translation>
    </message>
    <message>
        <source>Horizontal and Vertical </source>
        <translation type="vanished">Horizontale und Vertikale </translation>
    </message>
    <message>
        <source>Horizontal Only</source>
        <translation type="vanished">Nur Horizontale</translation>
    </message>
    <message>
        <source>Vertical Only</source>
        <translation type="vanished">Nur Vertikale</translation>
    </message>
    <message>
        <location line="-1499"/>
        <source>Start Measuring</source>
        <translation>Messung starten</translation>
    </message>
    <message>
        <source>Radians</source>
        <translation type="vanished">Radiant</translation>
    </message>
    <message>
        <source>Scale:</source>
        <comment>short</comment>
        <translation type="vanished">Skala:</translation>
    </message>
    <message>
        <source>Plot Units</source>
        <translation type="vanished">Grafik</translation>
    </message>
    <message>
        <source>User Scale</source>
        <translation type="vanished">Benutzer</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="vanished">&amp;Datei</translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation type="vanished">&amp;Zuletzt Verwendete öffnen</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="vanished">&amp;Bearbeiten</translation>
    </message>
    <message>
        <source>Significant Digits</source>
        <translation type="vanished">Signifikante Stellen</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="vanished">&amp;Ansicht</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="vanished">&amp;Hilfe</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation type="vanished">&amp;Öffnen...</translation>
    </message>
    <message>
        <source>&amp;Clear History</source>
        <translation type="vanished">&amp;Verlauf Löschen</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation type="vanished">&amp;Speichern...</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="vanished">&amp;Drucken...</translation>
    </message>
    <message>
        <source>&amp;Quit xyscan</source>
        <translation type="vanished">xyscan &amp;beenden</translation>
    </message>
    <message>
        <source>&amp;Paste Image</source>
        <translation type="vanished">&amp;Grafik einfügen</translation>
    </message>
    <message>
        <source>Crosshair Color...</source>
        <translation type="vanished">Fadenkreuzfarbe...</translation>
    </message>
    <message>
        <source>Marker Color...</source>
        <translation type="vanished">Markerfarbe...</translation>
    </message>
    <message>
        <source>Delete &amp;Last</source>
        <translation type="vanished">&amp;Letzten Eintrag löschen</translation>
    </message>
    <message>
        <source>Delete &amp;All</source>
        <translation type="vanished">&amp;Alles löschen</translation>
    </message>
    <message>
        <source>&amp;Comment...</source>
        <translation type="vanished">&amp;Kommentar...</translation>
    </message>
    <message>
        <source>&amp;Current Precision</source>
        <translation type="vanished">Aktuelle &amp;Präzision</translation>
    </message>
    <message>
        <source>Scan Screen</source>
        <translation type="vanished">Bildschirm scannen</translation>
    </message>
    <message>
        <source>&amp;About xyscan</source>
        <translation type="vanished">xyscan &amp;Info</translation>
    </message>
    <message>
        <source>xyscan &amp;Help</source>
        <translation type="vanished">xyscan &amp;Hilfe (in Englisch)</translation>
    </message>
    <message>
        <source>&amp;Tool Tips</source>
        <translation type="vanished">&amp;Tooltips</translation>
    </message>
    <message>
        <source>&amp;Check For Updates ...</source>
        <translation type="vanished">Nach neuster &amp;Version suchen ...</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+42"/>
        <location line="+957"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <source>Data Table</source>
        <translation type="vanished">Datentabelle</translation>
    </message>
    <message>
        <source>&amp;Data Table</source>
        <translation type="vanished">&amp;Datentabelle</translation>
    </message>
    <message>
        <source>Axis Settings</source>
        <translation type="vanished">Achsen definieren</translation>
    </message>
    <message>
        <source>Error Bar Scan Mode</source>
        <translation type="vanished">Fehlerbalkenmodus einstellen</translation>
    </message>
    <message>
        <source>Measure Tool</source>
        <translation type="vanished">Messwerkzeug</translation>
    </message>
    <message>
        <source>Scan area. The loaded graphics
is displayed here.</source>
        <translation type="vanished">Scan-Fenster. Die eingeladenen 
Grafiken werden hier angezeigt.</translation>
    </message>
    <message>
        <source>Help and Documentation for xyscan.</source>
        <translation type="vanished">Hilfe und Dokumentation für xyscan.</translation>
    </message>
    <message>
        <source>Status bar. Displays instruction during scanning.</source>
        <translation type="vanished">Statusleiste. Wichtige Instruktionen werden hier angezeigt.</translation>
    </message>
    <message>
        <source>Horizontal bar of crosshairs cursor.</source>
        <translation type="vanished">Horizontale Linie des Fadenkreuzes.</translation>
    </message>
    <message>
        <source>Vertical bar of crosshairs cursor.</source>
        <translation type="vanished">Vertikale Linie des Fadenkreuzes.</translation>
    </message>
    <message>
        <source>Marker for lower x-axis position.</source>
        <translation type="vanished">Marker für die kleinere x-Achsen Position.</translation>
    </message>
    <message>
        <source>Marker for upper x-axis position.</source>
        <translation type="vanished">Marker für die größere x-Achsen Position.</translation>
    </message>
    <message>
        <source>Marker for lower y-axis position.</source>
        <translation type="vanished">Marker für die kleinere y-Achsen Position.</translation>
    </message>
    <message>
        <source>Marker for upper y-axis position.</source>
        <translation type="vanished">Marker für die größere y-Achsen Position.</translation>
    </message>
    <message>
        <source>Measure Tool Window:
Allows to measure distances in plots and maps.</source>
        <translation type="vanished">Messwerkzeug Rahmen:
Erlaubt Abstände und Entfernungen 
in Grafiken und Karten zu messen.</translation>
    </message>
    <message>
        <source>Unit Selector:
Select the units in which the distance is displayed.
</source>
        <translation type="vanished">Einstellung der Einheiten:
Hier werden die Einheiten festgelegt in denen
Abstände und Entfernungen angezeigt werden.
</translation>
    </message>
    <message>
        <source>User Scale:
Allows to enter a scale in which the measured
distances will be expressed.</source>
        <translation type="vanished">Benutzer definierter Maßstab:
Erlaubt dem Benutzer seinen eigenen Maßstab
zu definieren in dem die Entfernungen angezeigt werden.</translation>
    </message>
    <message>
        <source>Starts and stops measuring.</source>
        <translation type="vanished">Entfernungsmessungen starten und beenden.</translation>
    </message>
    <message>
        <source>While measuring, displays the horizontal distance.</source>
        <translation type="vanished">Zeigt den horizontalen Abstand an.</translation>
    </message>
    <message>
        <source>While measuring, displays the vertical distance.</source>
        <translation type="vanished">Zeigt den vertikalen Abstand an.</translation>
    </message>
    <message>
        <source>While measuring, displays the radial distance.</source>
        <translation type="vanished">Zeigt den (radialen) Abstand an.</translation>
    </message>
    <message>
        <source>While measuring, displays the angle w.r.t. the horizontal axis.</source>
        <translation type="vanished">Zeigt den Winkel des gewählten Punktes zur horizontalen Achse an.</translation>
    </message>
    <message>
        <source>Select angle to be displayed in radians.</source>
        <translation type="vanished">Winkel in Radiant oder Grad anzeigen.</translation>
    </message>
    <message>
        <source>Set the zoom scale.</source>
        <translation type="vanished">Den Vergrößerungsfaktor einstellen.</translation>
    </message>
    <message>
        <source>Coordinates Window:
Displays cursor position in local
(pixel) and plot coordinates.
Shortcut is %1+C.</source>
        <translation type="vanished">Koordinatenfenster:
Zeigt die Cursorposition in lokalen
(Pixel) und Bild-Koordinaten an.
Die Abkürzung ist %1+C.</translation>
    </message>
    <message>
        <source>Displays the x coordinate of the cursor in pixel (screen) units.</source>
        <translation type="vanished">Zeigt die x-Koordinate des Cursors in Pixel (Bildschirm) -Einheiten an.</translation>
    </message>
    <message>
        <source>Displays the y coordinate of the cursor in pixel (screen) units.</source>
        <translation type="vanished">Zeigt die y-Koordinate des Cursors in Pixel (Bildschirm) -Einheiten an.</translation>
    </message>
    <message>
        <source>Displays the x coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation type="vanished">Zeigt die x-Koordinate des Cursors in Bild-Einheiten an.
Wenn der Punkt gescannt wird, wird diese Koordinate in
der Datentabelle gespeichert.</translation>
    </message>
    <message>
        <source>Displays the y coordinate of the cursor in plot units.
When the point is recorded (space key) this coordinate
gets stored in the data table.</source>
        <translation type="vanished">Zeigt die y-Koordinate des Cursors in Bild-Einheiten an.
Wenn der Punkt gescannt wird, wird diese Koordinate in
der Datentabelle gespeichert.</translation>
    </message>
    <message>
        <source>Axis Settings Window:
Use to set axes marker
and set log/lin scales.
Shortcut is %1+A.</source>
        <translation type="vanished">Definition der Achsen:
Hier werden die Werte der gesetzten Marker
eingegeben und festgelegt ob die Achsen linear
oder logarithmisch sind.
Abkürzung ist %1+A.</translation>
    </message>
    <message>
        <source>Set value of marker for the lower y-position.
Launches input dialog for the referring value in plot coordinates.
Shortcut is %1+3.</source>
        <translation type="vanished">Setzt den Wert des Markers für die untere (kleinere) Position auf der y-Achse.
Öffnet ein Fenster in dem der Wert in Bildkoordinaten eingegeben werden kann.
Abkürzung ist %1+3.</translation>
    </message>
    <message>
        <source>x-axis value in plot coordinates assigned to the low-x marker (read only).</source>
        <translation type="vanished">Momentaner Wert des unteren (kleineren) Markers auf der x-Achse.</translation>
    </message>
    <message>
        <source>x-axis value in plot coordinates assigned to the upper-x marker (read only).</source>
        <translation type="vanished">Momentaner Wert des oberen (größeren) Markers auf der x-Achse.</translation>
    </message>
    <message>
        <source>y-axis value in plot coordinates assigned to the low-y marker (read only).</source>
        <translation type="vanished">Momentaner Wert des unteren (kleineren) Markers auf der y-Achse.</translation>
    </message>
    <message>
        <source>y-axis value in plot coordinates assigned to the upper-y marker (read only).</source>
        <translation type="vanished">Momentaner Wert des oberen (größeren) Markers auf der y-Achse.</translation>
    </message>
    <message>
        <source>Check when x-axis on plot has log scale.</source>
        <translation type="vanished">Angeben ob die x-Achse eine logarithmische Skala hat.</translation>
    </message>
    <message>
        <source>Check when y-axis on plot has log scale.</source>
        <translation type="vanished">Angeben ob die y-Achse eine logarithmische Skala hat.</translation>
    </message>
    <message>
        <source>Rotate current plot (degrees).</source>
        <translation type="vanished">Hier kann man die Grafik rotieren (in Grad).</translation>
    </message>
    <message>
        <source>Scale (zoom in/out) the current plot.</source>
        <translation type="vanished">Hier kann die Grafik verkleinert und vergrößert werden.</translation>
    </message>
    <message>
        <source>Show dimension and depth of current plot.</source>
        <translation type="vanished">Zeigt die Dimension und Bit-Tiefe der Grafik an.</translation>
    </message>
    <message>
        <source>Error Bar Settings:
Define how to scan error bars.</source>
        <translation type="vanished">Einstellung des Fehlerbalkenmodus:
Hier wird festgelegt ob und wie die Fehlerbalken eingescannt werden.</translation>
    </message>
    <message>
        <source>Data Table Window:
Window displaying the data table that holds 
 all points (and error bars) scanned so far.</source>
        <translation type="vanished">Datentabelle:
Dieses Fenster zeigt eine Tabelle mit allen
bisher eingescannten Datenpunkten.</translation>
    </message>
    <message>
        <source>Open file to read in image.</source>
        <translation type="vanished">Datei die Grafik enthält öffnen.</translation>
    </message>
    <message>
        <source>Scan screen directly without loading image.
xyscan will turn semi-transparent.</source>
        <translation type="vanished">Grafiken direkt vom Bildschirm einscannen.
Das xyscan Fenster wird dafür transparent.</translation>
    </message>
    <message>
        <source>Save the scanned data in text file.</source>
        <translation type="vanished">Die gescannten Daten in einer Textdatei abspeichern.</translation>
    </message>
    <message>
        <source>Print the plot together with the scanned data.</source>
        <translation type="vanished">Die gescannten Daten zusammen mit der Grafik ausdrucken.</translation>
    </message>
    <message>
        <source>Quit xyscan.</source>
        <translation type="vanished">xyscan beenden.</translation>
    </message>
    <message>
        <source>Delete last scanned point from data table.</source>
        <translation type="vanished">Letzten Punkt (mit Fehlern) löschen.</translation>
    </message>
    <message>
        <source>Delete all scanned point from data table.</source>
        <translation type="vanished">Alle gespeicherten Punkte einschließlich  Fehlern löschen.</translation>
    </message>
    <message>
        <source>Write comment that will be added to the
scanned data when saved to file.</source>
        <translation type="vanished">Hier Kommentare eingegeben die zusammen
mit den Daten abgespeichert werden.</translation>
    </message>
    <message>
        <source>Paste image from clipboard.</source>
        <translation type="vanished">Grafik aus der Zwischenablage einfügen.</translation>
    </message>
    <message>
        <source>Clear list of recently opened files.</source>
        <translation type="vanished">Liste der zuletzt geöffneten Dateien löschen.</translation>
    </message>
    <message>
        <source>Shows scan precision at current cursor point.</source>
        <translation type="vanished">Zeigt die ungefähre Präzision am momentanen Punkt an.</translation>
    </message>
    <message>
        <source>Switch on/off tool tips.</source>
        <translation type="vanished">Tooltips an und abschalten.</translation>
    </message>
    <message>
        <source>Cursor Zoom</source>
        <translation type="vanished">Cursor-Zoom</translation>
    </message>
    <message>
        <source>&amp;Cursor Zoom</source>
        <translation type="vanished">&amp;Cursor-Zoom</translation>
    </message>
    <message>
        <location line="-1063"/>
        <source>xyscan - Comment</source>
        <translation>xyscan - Kommentar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The following comment will be written together with the data when saved to file or when printed:	</source>
        <translation>Dieser Text wird mit den Daten abgespeichert und erscheint im Ausdruck:	</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Crosshair Color</source>
        <translation>Fadenkreuzfarbe</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Marker Color</source>
        <translation>Markerfarbe</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Path Color</source>
        <translation>Pfadfarbe</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ready for screen scan. No markers set.</source>
        <translation>Bereit für den Bildschirm-Scan. Kein Marker definiert.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Stop Measuring</source>
        <translation>Messung beenden</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Measuring tool is on.</source>
        <translation>Messung ist im Gange.</translation>
    </message>
    <message>
        <location line="+309"/>
        <location line="+811"/>
        <source>Cannot scan yet. Not sufficient information available to perform the coordinate transformation. You need to define 2 points on the x-axis (x1 &amp; x1) and 2 on the y-axis (y1 &amp; y2).</source>
        <translatorcomment>Kann noch nicht scannen. Nicht genügend Information um die Koordinatentransformation auszuführen. Bitte 2 Punkte an der x-Achse und zwei an der y-Achse definieren.</translatorcomment>
        <translation>Kann noch nicht scannen. Es steht nicht genügend Information zur Verfügung, um die Koordinatentransformation auszuführen. Bitte 2 Punkte auf der x-Achse und zwei an der y-Achse setzten und definieren.</translation>
    </message>
    <message>
        <location line="-779"/>
        <source>Sorry no help available.
Help files are missing in this installation. Check your installation and reinstall if necessary.</source>
        <translation>Leider ist keine Hilfe erhältlich.
Die Dokumentation konnte nicht gefunden werden. Bitte Installierung überprüfen und gegebenenfalls neu installieren.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Image loaded, no markers set.</source>
        <translation>Grafik geladen. Marker noch nicht definiert.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimension:&amp;nbsp;&lt;td&gt;%1x%2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Depth:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha channel:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Device pixel ratio:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimensionen:&amp;nbsp;&lt;td&gt;%1x%2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Bit-Tiefe:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha-Kanal:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Gerätepixel-Verhältnis:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+53"/>
        <location line="+294"/>
        <source>&amp;%1  %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-159"/>
        <source>Images (</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>*.%1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="-999"/>
        <location line="+1019"/>
        <source>File &apos;%1&apos; does not exist.</source>
        <translation>Die Datei &apos;%1&apos; existiert nicht.</translation>
    </message>
    <message>
        <source>Plot Info and Adjustment</source>
        <translation type="vanished">Grafik Info und Einrichtung</translation>
    </message>
    <message>
        <source>Plot Info and Adjustment:
Window displaying info on the current plot and controls
that allows one to scale (zoom in/out) and rotate the plot.</source>
        <translation type="vanished">Grafik Info und Einrichtung:
Hier werden Informationen über die momentane Grafik angezeigt und
das Bild kann rotiert und vergrößert/verkleinert werden.</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Cannot load PDF file &apos;%1&apos;.</source>
        <translation>PDF-Datei &apos;%1&apos; kann nicht geladen werden.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Failed to extract page from PDF file &apos;%1&apos;.</source>
        <translation>PDF Grafik konnte nicht aus der PDF Datei &apos;%1&apos; extrahiert werden.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Raster Precision</source>
        <translation>Bildraster-Einstellung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF needs to be rastered to be displayed.
Enter pixel/inch:</source>
        <translation>Die PDF-Datei muss erst gerastert werden.
Pixel/Inch:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error rendering PDF file &apos;%1&apos;.</source>
        <translation>Die PDF Grafik in &apos;%1&apos; kann nicht dargestellt werden.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot load image from file &apos;%1&apos;. Either the file content is damaged or the image file format is not supported.</source>
        <translation>Die Grafik in &apos;%1&apos; kann nicht geladen werden. Entweder ist die Datei beschädigt oder das Format wird nicht unterstützt.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot load image from clipboard.
Either the clipboard does not contain an image or it contains an image in an unsupported image format.</source>
        <translation>Die Grafik in der Zwischenablage kann nicht geladen werden.
Entweder die Zwischenablage enthält keine Grafik oder das Format wird nicht unterstützt.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>xyscan - Print</source>
        <translation>xyscan - Drucken</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Save File</source>
        <translation>Datei abspeichern</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Text (*.txt);;Root Macro (*.C);;CSV (*.csv)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Datei %1 konnte nicht abgespeichert werden:
%2.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Data saved in &apos;%1&apos;.</source>
        <translation>Daten wurden in &apos;%1&apos; abgespeichert.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Error in calculating transformation.
Markers on x-axis are less than two pixels apart. Cannot continue with current settings.</source>
        <translation>Probleme mit der Berechnung der Koordinatentransformation.
Die Marker auf der x-Achse sind weniger als zwei Pixel voneinander getrennt.
Kann mit den momentanen Werten nicht arbeiten.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error in calculating transformation.
Markers on y-axis are less than two pixels apart. Cannot continue with current settings.</source>
        <translation>Probleme mit der Berechnung der Koordinatentransformation.
Die Marker auf der y-Achse sind weniger als zwei Pixel voneinander getrennt.
Kann mit den momentanen Werten nicht arbeiten.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error in calculating transformation.
Logarithmic x-axis selected but negative (or zero) values assigned to markers. Cannot continue with current settings.</source>
        <translation>Probleme mit der Berechnung der Koordinatentransformation.
Die x-Achse ist logarithmisch aber der zugewiesene Wert is 0 oder negativ.
Kann mit den momentanen Werten nicht arbeiten.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error in calculating transformation.
Logarithmic y-axis selected but negative (or zero) values assigned to markers. Cannot continue with current settings.</source>
        <translation>Probleme mit der Berechnung der Koordinatentransformation.
Die y-Achse ist logarithmisch aber der zugewiesene Wert is 0 oder negativ.
Kann mit den momentanen Werten nicht arbeiten.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>lower</source>
        <translation>unteren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>upper</source>
        <translation>oberen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the %1 %2-value at marker position:</source>
        <translation>Bitte den %1 %2-Wert der Markerposition eingeben:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>%1 is not a valid floating point number.</source>
        <translation>%1 ist keine korrekte Dezimalzahl.</translation>
    </message>
    <message>
        <location line="-820"/>
        <location line="+846"/>
        <source>Ready to scan. Press space bar to record current cursor position.</source>
        <translation>Zum Scannen bereit. Leertaste drücken, um die jetzige Cursorposition zu speichern.</translation>
    </message>
    <message>
        <location line="-719"/>
        <source>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimension:&amp;nbsp;&lt;td&gt;%1 x %2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Depth:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha channel:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Device pixel ratio:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;table border=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;Info:&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Dimensionen:&amp;nbsp;&lt;td&gt;%1x%2&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Bit-Tiefe:&amp;nbsp;&lt;td&gt;%3 bit&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Alpha-Kanal:&amp;nbsp;&lt;td&gt;%4&lt;tr&gt;&lt;td align=&quot;right&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;Gerätepixel-Verhältnis:&amp;nbsp;&lt;td&gt;%5&lt;/table&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Cannot open PDF file &apos;%1&apos;.</source>
        <translation>PDF-Datei &apos;%1&apos; kann nicht geladen werden.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>PDF file &apos;%1&apos; is empty (no pages).</source>
        <translation>PDF-Datei &apos;%1&apos; is leer (enthält keine Seiten).</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select Page</source>
        <translation>Seite auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The PDF file has %1 pages.
Select the desired page:</source>
        <translation>Die PDF-Datei hat %1 Seiten.
Wählen Sie die gewünschte Seite aus:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>PDF can be scaled before loading.
Original resolution is %1 x %2.
Enter scale:</source>
        <translation>PDF kann vor dem Laden skaliert werden.
Die ursprüngliche Auflösung beträgt %1 x %2.
Skalierungsfaktor eingeben:</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Data saved as ROOT macro &apos;%1&apos;.</source>
        <translation>Daten wurden als ROOT Macro &apos;%1&apos; abgespeichert.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Data saved as csv file &apos;%1&apos;.</source>
        <translation>Daten wurden in &apos;%1&apos; im CSV Format abgespeichert.</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 of 4 markers set.</source>
        <translation>%1 von 4 Marker platziert.</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Estimated precision at current point:
dx = +%1  -%2
dy = +%3  -%4</source>
        <translation>Geschätzte Präzision am momentanen Punkt
dx = +%1  -%2
dy = +%3  -%4</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot determin precision yet.
Place all 4 markers first.</source>
        <translation>Kann die Präzision noch nicht abschätzten.
Bitte erst alle 4 Marker setzen und Werte definieren.</translation>
    </message>
</context>
</TS>
